\section{Introduction}
This is the user manual for the Intensity Interferometry on RFSoC project. 
It is separated into 3 sections: \begin{itemize}
	\item Connecting the HTG-ZRF8 card inputs and to the network
	\item Flashing and booting the RFSoC bitstream, to use another bitstream than the default one.
	\item Use the onboard daq software to start the data acquisition and output it to the network.
\end{itemize}
        For all these steps, you will need: an HTG-ZRF8 board with its power supply, coax input cables, a micro-USB connector for serial, a JTAG adapter to program the SoC and an Ethernet cable to connect the board to the network.
     
\section{Board layout and connection}
\label{s:board_layout_connection}
	The board used in this project is the HitechGlobal HTG-ZRF8. 
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/rf_soc_up}
		\caption{RFSoC from the top. Letters indicate connection points}
		\label{fig:rfsocup}
	\end{figure}

	First, connect all the needed cables to the board (Figure \ref{fig:rfsocup}).
	Connect the input signal cables to the ports marked as A (see bitstream information for more details about which are used).
	The micro-USB cable for serial console has to be connected to the second USB port from the left on the top of the card (marking B).
	Then, connect the JTAG interface to the JTAG connector (marking C).
	Check the presence of the SD card in slot F.
	Finally, connect an Ethernet cable in the connector E.
	The power will be connected later in the port D.
	
	
\section{Programming and booting the Board}
	At this stage of the project, the bitstream embedded into the bootloader is not up to date and should not be used.
	To change it, follow these steps.
	You will need: Vivado, a working bitstream and a serial interface software (minicom, PuTTY or equivalent).\\
	\\
	Connect the board as instructed in \ref{s:board_layout_connection}.
	Do not plug the power yet. 
	Prepare your console application with the following parameters:
	\begin{itemize}
		\item Serial port: this is the one on which the card will appear.\footnote{ if it is your first time, connect the board onto your computer, plug it in and find out the port. When you found it, power down the card safely by opening the serial interface, logging in (user: root, password: root) and typing \mintinline{bash}{halt}. This will safely power down the board.}
		\item Baud rate (or speed): 115200.
	\end{itemize}
	\paragraph*{Serial Connection}When ready, plug in the power of the board, and start your serial interface. 
	You cannot start the interface before powering up as the usb-to-serial chip of this board is powered by the main power supply and not the USB. 
	When started, open the serial interface and wait until the text \mintinline{bash}{Hit any key to stop autoboot: } appears. 
	At this moment, hit any key to stop the autoboot (Figure \ref{fig:puttybootstop}). 
	We want to program the board before loading the operating system, as Vivado is trying to connect to the non existing debug probes on the board.
	This would slow down or crash Linux most of the times.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/putty_boot_stop}
		\caption{Stop the autoboot process when prompted.}
		\label{fig:puttybootstop}
	\end{figure}
	

	\paragraph*{Hardware Manager}
	Now, start Vivado and open the Hardware manager. 
	Click on "Open target" (Figure \ref{fig:vivadoopentarge}) and then "Auto Connect"(Figure \ref{fig:vivadoautoconnect}).
	Wait for the connection to be established and click on "Program the device". 
	In the newly opened window, select the bitstream you want to use and click "Program".
	Wait for the programming to finish, right click on the JTAG server (localhost) and select "Close Server" (Figure \ref{fig:vivadocloseserver}).
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/vivado_open_targe}
		\caption{Vivado: Open Target}
		\label{fig:vivadoopentarge}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/vivado_autoconnect}
		\caption{Vivado: Autoconnect}
		\label{fig:vivadoautoconnect}
	\end{figure}
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/vivado_close_server}
		\caption{Vivado: Close Server}
		\label{fig:vivadocloseserver}
	\end{figure}
	
	
	
	\paragraph*{Boot}
	Now, go back to the serial interface to boot the SoC.
	Type \mintinline{bash}{boot}, press enter and wait for the login prompt as in figure \ref{fig:puttybootcontinue} to appear. 
	The username and password are "root".
	
	\begin{figure}[H]
		\centering
		\includegraphics{figs/putty_boot_continue}
		\caption{Type \mintinline{bash}{boot} to continue the booting process after the bitstream programming and the JTAG server closed.}
		\label{fig:puttybootcontinue}
	\end{figure}
	
\section{Usage of the Interferometry data acquisition server}
	When booted up, the board is not yet ready for data acquisition. 
	The ADC PLL (clock) must be configured and started for the system to work.
	Once it is done, the server can be launched to send data to a client. 
	
	\paragraph*{PLL configuration} 
	To start the PLL, a small script can be found in the \mintinline{bash}{/home/schuler} folder called \mintinline{bash}{launch_clock.sh}
	This script calls the TICS-to-GPIO application with the configuration file for the PLL as argument.
	On the board, 4 configuration files are provided:
	\begin{itemize}
		\item 500MHz
		\item 1GHz
		\item 2GHz
		\item 4GHz
	\end{itemize}
	
	The same result can be achieved by calling directly the program: 
	
	\mintinline{bash}{./ticspro_to_gpio 0xA0020000 ./HexRegisterValues2GHz.txt}. 
	If needed, change \mintinline{bash}{0xA0020000} with the register address of the PLL GPIO from the design.
	To generate the HexRegisterValues files, see project report.
	
	\paragraph*{Acquisition and data server}
		To launch the acquisitions and the data export server, a simple command has to be run.
		First, go to \mintinline{bash}{cd /home/etienne/gpu-sii/accums_readout} and run \mintinline{bash}{make} to be sure to be working on the last version.
		Run the system with the command \mintinline{bash}{./daq}.
		
		Multiple options are available as arguments:
		\begin{itemize}
			\item -delayA x to -delayF x: allows to add x clock cycles of delay for the entries A to F. As the data is grouped in batches of 8 samples, it delays $x \cdot 8$ samples.
			\item -capture-signal : enables the signal capturing feature, if available on the bitstream.
			\item -nb\_channels x : allows to override the default number of input channels sent in the general information structure (defaults to 3).
			\item -batch\_size x : allows to change the batch size of the accumulators. The value configured is $1 << x$ (equivalent to $2^x$). 
		\end{itemize}
		When launched, the data can be captured using the right client \footnote{See report for more info on how to recover the data}.
		To know the IP address of the board on the network, use \mintinline{bash}{ip a} in the command line (serial). 
		By default, SSH is enabled, but root login is disabled. 
		If needed, create your user account and add your ssh key to login to the board.
		
	\paragraph*{Led status}
		On the top of the board is a group of 8 LEDs next to the USB port. 
		The four rightmost are driven by the internal logic of the FPGA. 
		They indicate the internal state of the clocks and possible over voltage on the inputs of the RF data converter. 
		The LEDs are numerated from the rightmost to the leftmost, and their signification is shown in table \ref{tab:leds}.
		
		\begin{table}[H]
			\begin{tabular}{|c|c|c|}
				\hline
				LED & Signification & Default \\
				\hline
				0 & ADC inputs over voltage & Should be off \\
				\hline
				1 & Internal AXIS PLL locked & Should be on \\
				\hline
				2 & ADC clock & Should blink: Freq = GHz/2 \\
				\hline
				3 & ADC PLL locked & Should be on \\
				\hline
			\end{tabular}
			\caption{LED signification}
			\label{tab:leds}
		\end{table}
	
	