\subsection{Sigma accumulators}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\linewidth]{figs/block_sigma}
	\caption{Block sigma accumulators}
	\label{fig:blocksigma}
\end{figure}

Sigma Accumulator is the module in which the accumulation of $x$ and $x^2$ from equation \ref{equ_sigmas_split} happens.
Every clock cycle, data gets transmitted from the ADC to this module using the AXI Stream protocol. 
The results are then available through the AXI memory mapped bus.
The module needs to be able to do the calculation in real-time. 
However, real-time for this project does not mean that we cannot have latency. 
In this context, it means that data gets processed in real-time but takes multiple steps before outputting a result. 
These steps can happen in parallel, which allows this system to get data at each clock cycle.
The module also needs to stop after a given number of samples accumulated (batch size), to avoid overflowing the accumulators and to give the possibility to exclude unusable data (for example caused by light bursts).
The default value for the batch size used in this project has been arbitrarily set to $2^{20}$ samples but needs to be programmable to adapt to multiple frequencies and configurations.

The initial idea was to do the division by the number of samples ($n$ in the equation \ref{equ_sigmas_split}) on hardware (not implemented for now to debug and observe the individual results). 
Therefore the number of samples accumulated should remain a power of 2. 
This would allow the division to be implemented as a simple bit-shift.

%As reminder, the data is sent in batches of samples.

\subsubsection{Architecture}
	\begin{figure}[h]
		\centering
		\includegraphics[width=1.0\linewidth]{figs/generated/Sigma_Accumulators-Overview}
		\caption{Schematic of the sigma accumulators Module}
		\label{fig:sigmaaccumulatorsserial-overview}
	\end{figure}
	
	The module (figure \ref{fig:sigmaaccumulatorsserial-overview}) is split into multiple parts.
	
	First, a counter keeps track of the number of samples accumulated, the number of total accumulations done (batches) and the output and reset signal.
	This last one allows to reset the accumulators and output their respective results.
	As each sigma accumulators module has its own counter, they all have to count synchronously. 
	This is done via the \mintinline{vhdl}{sync} input.
	
	The second part is comprised of the accumulators. 
	They are sub-modules that are generated during synthesis (see code below) to provide the right number of accumulators depending on the number of data outputted in parallel from the ADC (info provided via the generic \mintinline{vhdl}{samples_per_adc}).
	
	\begin{minipage}{\linewidth}
		\inputminted[firstline=224, lastline=251]{vhdl}{code/HW/Sigma_accumulators.vhd}
	\end{minipage}

	The two accumulators are sub-modules that implement the accumulation and the squared accumulation.
	The data sent to these accumulators is converted from 16 bits (output of the ADCs) to the 12 bits of usable data in it (15 down to 4).
	
	The last part is the AXI Memory Mapped interface.
	This interface allows to read out the results of all accumulators and also configure the batch size (number of samples to accumulate) at runtime.

\subsubsection{Counter}
	The counter has multiple functions:
	it counts the number of samples accumulated (\mintinline{vhdl}{i_counter}), the number of data batches (\mintinline{vhdl}{i_batch_count}) done until now, and sends the reset signals (\mintinline{vhdl}{i_sload}) to the accumulators. 
	These signals for all the sub-modules as these have to be delayed correctly to have the results always in sync (figure \ref{fig:countertimingdiagram}). 
	All this is done in each accumulators and has to be synchronized in order to link all the results together at the end. 
	To do so, the sync signal is used to reset all the counters simultaneously. 
	The counter is implemented using 3 processes.
	
 	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{figs/counter_timing_diagram}
		\caption{Timing diagram of the counters}
		\label{fig:countertimingdiagram}
	\end{figure}

%{signal: [
%	{name: 'clk', wave: 'P........|......'},
%	{name: 'sync', wave: '01.......|......'},
%	{name: "i_counter_sigma", wave :'2.2222222|222222', data:['0', '1', '2', '3', '4', '5', '6', '7', 'MAX','0','1','2','3','4']},
%	{name: "i_batch_count", wave :'2....2...|.....2', data:['0', '1', '2', '3', '4', '5', '6', '7', '8']},
%	{name: "i_sload(0)", wave: '0.10.....|..10..'},
%	{name: "i_sload(1)", wave: '0..10....|...10.'},
%	],
%	
%	config: {hscale: 2}, 
%	head: {tick:0}
%}

	
	\paragraph*{}The first process (\mintinline{vhdl}{p_counter}) counts the number of elements in the batch.
	The counter, \mintinline{vhdl}{i_counter_sigma}, value is incremented each \mintinline{vhdl}{adc_clock} rising edge and reset to 0 either when the sync signal is '0', or when the value arrives at \mintinline{vhdl}{i_batch_size - 1}.
	
	%\begin{minipage}{\linewidth}
	%	\inputminted[firstline=193, lastline=204]{vhdl}{code/HW/Sigma_accumulators.vhd}
	%\end{minipage}
	
	The second process (\mintinline{vhdl}{p_batch_count}) keeps track of the batch number of the output data.
	It updates the counter when \mintinline{vhdl}{batch_size} samples got to the module. 
	Because of latencies in the accumulation sub-modules, the batch counter is not incremented when \mintinline{vhdl}{count = max_count}, but when \mintinline{vhdl}{count = 3}, as all the output data will be updated at this point in time only.
	
	 The last process (\mintinline{vhdl}{p_output_storage_and_sload}) puts the \mintinline{vhdl}{i_sload} signal to 1 when the data must be outputted. 
	 As the two accumulator processes don't take the same latency to output data, the sload signal is separated for them.
	 The \mintinline{vhdl}{sload(0)} signal is set to \mintinline{vhdl}{'1'} and sent to the single accumulator at \mintinline{vhdl}{count = 0} and the \mintinline{vhdl}{sload(1)} signal at \mintinline{vhdl}{count = 1} to the squared accumulator as it takes one more latency.
	 These two accumulators will be described in the next sections.
	 \\
	 \\
	 This module is kept in three different processes for the sake of simplicity, as \mintinline{vhdl}{p_counter} and \mintinline{vhdl}{p_batch_counter} are reused in other modules. 

\subsubsection{Sigma single accumulator} 
	This accumulator (figure \ref{fig:sigmaaccumulators-simple-accumulator}) takes an ADC sample as input and sums it to the previous value: $ out = \sum^N_{i=0}{in_i} $.
	The module uses a DSP48 cell of the RFSoC to make this accumulation. 
	Even though the default implementation would have been a simple LUT adder, the use of the DSP was forced. 
	This allows to have a predictable timing to get the result as it takes 2 clock cycles from the input of the data \mintinline{vhdl}{axis_tdata} to the output of the result \mintinline{vhdl}{adder_out} (see figure \ref{fig:sigmaaccumulators-timings}). 
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=1.0\linewidth]{figs/generated/Sigma_Accumulators-Simple-accumulator}
		\caption{Accumulator module}
		\label{fig:sigmaaccumulators-simple-accumulator}
	\end{figure}
	
	\begin{figure}[H]
		\centering
		\includegraphics[width=\linewidth]{figs/sigma_acc_timing}
		\caption{Timing of the simple accumulator. \mintinline{vhdl}{axis_tdata} is the input data, which is copied in the DSP input register (\mintinline{vhdl}{a_reg}). This data is then added to \mintinline{vhdl}{old_result} and the result is stored in \mintinline{vhdl}{adder_out}. When the batch is finished, the \mintinline{vhdl}{adder_out} data is copied to \mintinline{vhdl}{reg_out}.}
		\label{fig:sigmaaccumulators-timings}
	\end{figure}

%{signal: [
%	{name: 'clk', wave: 'P........|......'},
%	{name: 'sync', wave: '01.......|......'},
%	{name: "i_counter_sigma", wave :'2.2222222|222222', data:['0', '1', '2', '3', '4', '5', '6', '7', 'MAX','0','1','2','3','4']},
%	{name: "i_batch_count", wave :'2....2...|.....2', data:['0', '1', '2', '3', '4', '5', '6', '7', '8']},
%	{name: "i_sload(0)", wave: '010......|..10..'},
%	{name: "sload_reg", wave: '0.10.....|...10.'},
%	{name: "axis", wave :'x222222222|22222', data:['1','2','3','4','5','6','7','8','9']},
%	{name: "i_reg_a[0]", wave :'2.22222222|22222', data:['0', '1','2','3','4','5','6','7','8']},
%	{name: "a_reg", wave :'2..2222222|22222', data:['0', '1','2','3','4','5','6','7']},
%	{name: "adder_out", wave: '2...222222|22222', data:['0', '1','3','6','10','15', '21']},
%	{name: "old_result", wave: '2...222222|22222', data:['0', '1','3','6','10','15']},
%	{name: "reg_out", wave: 'x...2.....|....2', data:['0', 'sum','','6','10','15', '21']},
%	],
%	
%	config: {hscale: 2}, 
%	head: {tick:0}
%}

	
	The goal of this module is to accumulate N values received. 
	To do so, the reset signal performs two steps: it stores the final value of the accumulator in a register and it resets the accumulator value to 0. 
	This signal is generated by the sigma accumulator top module when \mintinline{vhdl}{count = 1}, as it take 2 clock cycles to propagate to the reset signal in this module. 
	This allows to output the result at the right time and to start the accumulation with the value gotten when \mintinline{vhdl}{count = 0}, considering the DSP propagation delay.
	As the result is copied at each batch end in a register, it is important to read this value at the right time, otherwise there is a risk of losing the data.
	Having the reset signal handled by the sigma accumulator top-level module allows changing the batch size dynamically without needing to propagate the configuration to the sub-modules. 
	
	


\subsubsection{Sigma squared accumulator}
	The second accumulator is accumulating the squared value of the input signal: $ out = \sum^N_{i=0}{a^2_i} $.
	This module is also based on the DSP48 cell to multiply and accumulate (figure \ref{fig:sigmaaccumulators-squared-accumulator}). 
	\begin{figure}[H]
		\centering
		\includegraphics[width=1.0\linewidth]{figs/generated/Sigma_Accumulators-Squared-accumulator}
		\caption{Squared accumulator module}
		\label{fig:sigmaaccumulators-squared-accumulator}
	\end{figure}
	As this implementation does also a squaring of the input value, the output is delayed by one clock cycle compared to the single accumulator, as shown in figure \ref{fig:sigmasacctiming}.
	In total, there is a three clock cycle delay between the data input and the resulting output. 
	Although the DSP configuration is different, the rest of the module is the same as the single accumulator. 



	\begin{figure}[H]
		\centering
		\includegraphics[width=0.7\linewidth]{figs/sigma_sacc_timing}
		\caption{Timing of the squared accumulator. \mintinline{vhdl}{axis_tdata} is the input data, which is copied in the DSP input register (\mintinline{vhdl}{a_reg}). This data is then squared in \mintinline{vhdl}{mult_reg} and finally added to \mintinline{vhdl}{old_result}. The result is stored in \mintinline{vhdl}{adder_out}. When the batch is finished, the \mintinline{vhdl}{adder_out} data is copied to \mintinline{vhdl}{reg_out}.}
		\label{fig:sigmasacctiming}
	\end{figure}
	

%{signal: [
%	{name: 'clk', wave: 'P........|......'},
%	{name: 'sync', wave: '01.......|......'},
%	{name: "i_counter_sigma", wave :'2.2222222|222222', data:['0', '1', '2', '3', '4', '5', '6', '7', 'MAX','0','1','2','3','4']},
%	{name: "i_batch_count", wave :'2....2...|.....2', data:['0', '1', '2', '3', '4', '5', '6', '7', '8']},
%	{name: "i_sload(0)", wave: '010......|..10..'},
%	{name: "sload_reg", wave: '0.10.....|...10.'},
%	{name: "axis", wave :'x222222222|22222', data:['1','2','3','4','5','6','7','8','9']},
%	{name: "i_reg_a[0]", wave :'2.22222222|22222', data:['0', '1','2','3','4','5','6','7','8']},
%	{name: "a_reg", wave :'2..2222222|22222', data:['0', '1','2','3','4','5','6','7']},
%	{name: "mult_reg", wave :'2..2222222|22222', data:['0', '1','2','3','4','5','6','7']},
%	{name: "adder_out", wave: '2....222222|2222', data:['0', '1','3','6','10','15', '21']},
%	{name: "old_result", wave: '2....222222|2222', data:['0', '1','3','6','10','15']},
%	{name: "reg_out", wave: 'x....2.....|...2', data:['0', 'sum','','6','10','15', '21']},
%	],
%	
%	config: {hscale: 2}, 
%	head: {tick:0}
%}

\subsubsection{AXI slave registers}
\label{ss:Sigma_acc_AXI_MM_Desc}
	To communicate with the Linux operating system, the modules need to have an AXI interface.
	The interface of the sigma accumulator can be accessed to configure the sample size to accumulate and read the accumulated values. 
	This interface is AXI4 compatible \footcite{amba_axi4_specifications} with a 64 bit data width.
	
	The register map used for this module is shown in table \ref{table:registermap_sigma}.
	Each individual accumulator result is accessible through the registers 1-8 and 9-16.
	As the accesses are done on 64 bit width, the addresses increment in every 8th value (0x00->0x08->0x10->0x18 and so on).

	\begin{table}[H]
		\caption{Sigma accumulator register map}
		\label{table:registermap_sigma}
		\begin{tabular}{|c|c|c|c|}
			\hline
			Register number & Address & Read (64 bits) & Write (64 bits) \\
			\hline
			0 & 0x00 & Batch size & Batch size \\
			\hline
			1-8 & 0x08-0x40 & Simple accumulator result & -- \\
			\hline
			9-16 & 0x48-0x80 & Multiplication accumulator results & --  \\
			\hline
		\end{tabular}
	\end{table}

