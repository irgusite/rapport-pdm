\section{General Architecture}


	
	\subsection{Telescopes}
	\label{ss:telescopes}
		The telescope on which this experience should be used is the MAGIC (Major Atmospheric Gamma Imaging Cherenkov) telescope (Figure \ref{fig:magicmirror}) located in La Palma. 
		It is a system comprised of two telescopes called Imaging Atmospheric Cherenkov Telescope.
		This kind of telescope is comprised of an array of mirrors, reflecting the light to a sensor mounted on the focal point.
		The sensor used in MAGIC is an array of photomultiplier tubes.
		
		Using this kind of telescope has multiple benefits for this project.
		First, the incoming light can be focused on one photomultiplier as each mirror can be moved to focus on the same spot.
		The second benefit is that such a telescope can do a 0 meter baseline correlation. 
		This means that half of the mirrors send the light to one sensor, and the other half to a second sensor. 
		This allows to virtually have two telescopes on one spot.
		
		\begin{figure}
			\centering
			\includegraphics[width=\linewidth]{figs/Magicmirror}
			\caption{MAGIC on La Palma, Canary Islands. (Photo by:  Pachango Source: https://en.wikipedia.org/wiki/MAGIC\_(telescope)\#/media/File:Magicmirror.jpg)}
			\label{fig:magicmirror}
		\end{figure}
		
	% Quelles données on recoit?
	\subsection{Data input}
		The intensity signal is generated via the photomultipliers of the telescope.
		These output a current proportional to the quantity of photons received.\footcite{magic_technical_implementation}
		This current is then amplified and fed to a laser diode in order to transmit the signal further via fiber optic.
		At the other end, a photodiode and amplification system output a signal varying in voltage.
		The RFSoC then converts this voltage to a 12 bits signed integer digital value, stored as a 16 bits value with the lower bits not used. 
		(An important point discovered during this project: these bits are not 0, there can be noise on them).
		This value is then temporarily stored in a FIFO memory and, after that, sent to the calculation modules using the AMBA AXI Stream protocol. \footcite{zynq_rfdata_converter_documentation}
		To alleviate the need to have a very high frequency FPGA to keep up with the 4GHz maximum smapling rate, the data is sent in form of packets.
		These packets (batches) can contain up to 8 samples depending on the configuration (see figure \ref{fig:correlationaccum-signal-capture}). 
		This allows to reduce the FPGA working frequency to a maximum of 500MHz.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/generated/Correlation_accum-Signal-capture}
			\caption{Schematic of the signal capture. Arrow : the signal ; dotted lines : the capture sample times ; colored rectangles : the samples batches containing each 8 samples.}
			\label{fig:correlationaccum-signal-capture}
		\end{figure}
		
		
	\subsection{Correlation equation}
		The project aims to implement the correlation calculations as efficiently as possible in order to allow high-speed calculations and diminish the amount of data to store. 
		The basic correlation equation is :
		\begin{equation}
			\label{equ_correlation_origin}
			\rho(d) = \frac{1}{n}\sum^n_{i=0} { \frac{x_i - \langle x \rangle}{\sigma_x} \frac{y_{i+d} - \langle y \rangle}{\sigma_y} }
		\end{equation}
		With
		\begin{equation}
			\label{equ_sigmas_origin}
			\sigma = \sqrt{\frac{\sum^n_{i=0}{(x - \langle x \rangle)^2}}{n}}
		\end{equation}
		Here, $x_i$ and $y_i$ are the intensity input signals coming from the telescopes and $n$ is the number of samples we want to capture and calculate. 
		$\sigma_x$ and $\sigma_y$ are the standard deviations. Finally, $\langle x \rangle$ and $\langle y \rangle$ are the means of the signal. 
		In this form, the equation cannot easily be implemented into an FPGA and disallows DSP usage.
		It heavily relies on division at each step of the sum, which are resources and time intensive. 
		To do such a division with the hardware we use, 56 clock cycles would be necessary to output a single result, making real time calculation impossible.
		To integrate the equation on the hardware, we need to rewrite it in order to extract the divisions.
		
		\begin{equation}
			\label{equ_correlation_split}
			\rho(d) = \frac{1}{n \sigma_x \sigma_y}( \sum^n_{i=0}{(x_i \cdot y_{i+d})} - n \langle x \rangle \langle y \rangle)
		\end{equation}
		
		In equation \ref{equ_correlation_split}, the division is extracted from the sum and is calculated after all sums. 
		It allows us to consider the sums as accumulations and to do divisions only once every complete accumulation. 
		This rewriting needs also to be done for the $\sigma$:
		
		\begin{equation}
			\label{equ_sigmas_split}
			\sigma = \sqrt{\frac{1}{n}\sum^n_{i=0}{x_i^2 - (\frac{1}{n} \sum^n_{i=0}{x_i})^2}}
		\end{equation}
		
		In equation \ref{equ_sigmas_split}, we cannot extract the sums from the square root, but this rewriting still allows us to separate it into two implementable accumulations.
		The only drawback of the equation in this form is that it is not numerically stable since the sum is done before the mean value is removed. 
		In certain situations, this could lead to very big numbers and overflow the system.
		But this is taken into account in this project and overflow is not a risk, and thus this equation can be used.
		However, this downside will not be a problem in this project : as the input values are 12 bits signed integers, limiting the sum to $n=2^{24}$ allows to have all results with a maximum bit width of 48 bits, which is the maximum size of the DSP modules. 
		Thus, overflow is not a risk here and this equation can be used.

		
	% Comment on va le faire en gros
	
		
	\subsection{High level implementation}
	\label{ss:high_level_implementation}
		% explain the final logic used
		The figure \ref{fig:high-level-page-1} shows the general architecture used in this project. 
		As described in section \ref{ss:telescopes}, the telescopes send the intensity value back to the central location using fiber optics.
		In the central location, multiple photodiodes and amplifiers convert the light intensity into a voltage.
		This one is sent to the RFSoC.
		Once in the SoC, the voltage is converted via the Analog to Digital Converters (ADC) in a datastream sent to the different correlation modules. 
		These modules output results in a timely manner that is then retrieved by the embedded CPU and stored in temporary structures.
		These are then send over the network to another computer or server which will store and use the data.
		This project only focuses on the SoC implementation.
		 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/generated/High-Level}
			\caption[General overview]{General architecture overview \small{(photo source: Daniel López/IAC)}}
			\label{fig:high-level-page-1}
		\end{figure}
	\newpage
		\paragraph*{SoC Top Level}
			Figure \ref{fig:new-architecture-6-input-channels} shows the general block diagram of the system for 3 input channels.
			This design could be used with up to 6 channels. 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{"figs/generated/New Architecture-Actual-state"}
			\caption{General view of the SoC implementation. Here 3 input channels are shown. A signal capture module is show on input A and B as well as the signal injector.}
			\label{fig:new-architecture-6-input-channels}
		\end{figure}
			
			Starting with the CPU side, an acquisition program is responsible for getting the results of the accumulators and store them in structures to be exported via network.
			This application also uses the AXI bus to configure some parts of the design, like the number of data batches to correlate, the input delays, or the use of the signal capture module.
			This interfacing is provided by a C++ class connecting to the hardware and providing an abstraction layer.
			\\
			\\
			Inside programmable logic region, the hardware is separated in logical modules. 
			Going from the signal input on the left of the block diagram \ref{fig:new-architecture-6-input-channels}, the telescopes signals input each their individual ADC.
			These outputs data using the AXI4 Stream protocol, and the data goes first into delayer modules.
			These modules are here to allow moving the correlation window in order to recenter the data and correct mismatched cable length.
			\\
			\\
			After the delayer, the signal is sent to the so called sigma accumulators and correlation accumulators.
			Regarding the sigma accumulators, they do the sum of the signal for the equation \ref{equ_sigmas_split}.
			More precisely, this module does two sums: $\sum_{i=0}^{N}{x_i}$ and $\sum_{i=0}^{N}{x^2_i}$.
			The other module, the correlation accumulator, does the sum of two signal for the equation \ref{equ_correlation_split}.

			This module, making two signals shift one with the other, needs to store old signal data to do: $\sum_{i=0}^{N}{x_i \cdot y_{i+d}}$, as d spans from 0 to 100 in this design.
			These modules then store their result in a register that can be read via the AXI4 interface. 
			
			In total, there is one sigma module per channel, and $ N_c = \frac{N_{corr} \cdot (N_c - 1)}{2}$ modules for the correlation, with $N_c$ the number of channels.
			This shows that the number of correlation rapidly grows, contrary to the sigmas. 
			As explained in the next section, this will be a limitation for the design.
			\\
			\\
			In parallel to the correlation and sigma modules, a signal capture module allows to capture an input signal.
			This enables the possibility to check visually the input signal, and also to confirm the correct working of the other modules. 
			\\
			\\
			A last module, shown in the figure \ref{fig:new-architecture-6-input-channels} is the signal injector. 
			This module allows to inject a constant instead of the ADC data stream to the downstream modules. 
			This allows to test the working of the other modules with a known input value.
			
			
		
		
		\paragraph*{Signal synchronization}
			Normally, for the equation \ref{equ_correlation_split}, all the data taken into account for the standard deviation, mean values and correlation, are the same.
			Nevertheless, the architecture of this project uses some data in the correlation that will not be present in the corresponding standard deviation and mean values.
			The figure \ref{fig:sigmacorrelationdataproblem} shows the small history of the previous dataset kept when a new correlation starts.
			The mean and sigma values does not take these into account. 
			This means that W (the correlation window size) values are not taken into account either.
			This is not very problematic as it represents $\frac{1}{10000}$ of the values and is calculated in the previous mean and sigma results. 
		
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.7\linewidth]{figs/generated/sigma_correlation_data_problem}
				\caption{Sigma accumulator and correlation accumulator are not taking precisely the same data.}
				\label{fig:sigmacorrelationdataproblem}
			\end{figure}
		

	\subsection{Hardware limitations}
	
		To implement the correlation equation, we need to define how each part is calculated and on what kind of hardware.
		One crucial point is considering which hardware is available and how to use it most efficiently. 
		The goal is to calculate the correlation in real-time and save the results.
		The SoC used in this project is a ZU28DR.
		This chip contains 4272 DSP Slices, no FPU (floating point unit), 930'300 Logic Cells and multiple Mb of RAM (block RAM and distributed RAM). 
		The limitation of this chip is the fact that there is no FPU and a limited amount of DSP Slices available for the design.
		The consequences are that the division and square roots of the equations cannot be easily implemented on the FPGA.
		In order to overcome this, it has been decided to do the floating point operations on an external CPU once the data has been saved. 
		As a result, this leaves only the accumulations to implement.
		
		\paragraph*{}
		Starting with equation \ref{equ_sigmas_split}, the sums can be implemented on hardware using Digital Signal Processing units (DSP) configured as accumulate and multiply-accumulate modules.
		For the division by n, if the value is kept as a power of 2, the result can be calculated using bit shifting.
		As multiple samples are given by the ADC simultaneously, the design must be parallelized. 
		These accumulators (later called sigma accumulators) depend on the number of input channels and the number of data pushed in parallel from the ADC. 
		The final number of DSP used for the sigma accumulators is given by: $ N_\sigma = N_{parallel\_adc} \cdot N_{channels}$.
		
		\paragraph*{}
		For the correlation equation \ref{equ_correlation_split}, as for the sigma accumulators, only the sum will be implemented on the hardware.
		The division being complicated and needing the values of the sigmas, this part is done on another device.
		This leaves the sum ($ \sum^w_{i=0}{(x_i \cdot y_{i+d})} $) to implement. 
		
		As it takes the signal from 2 different inputs and calculates the entire correlation window in real-time, this implementation takes up many resources.
		For instance, the system needs one DSP per correlation window value (d), multiplied by the number of ADC samples per batch.
		Depending on the number of inputs, the number of correlations is given by: $ N_c = \frac{N_c \cdot (N_c - 1)}{2}$.
		Finally the size of the correlation window and the number of parallel data streams must be added to the equation which gives the number of DSP needed for the design:
		\begin{equation}
			N_{DSP} = \frac{N_c \cdot (N_c - 1)}{2} \cdot W \cdot N_{parallel\_adc}
		\end{equation}
		with $N_c$ the number of input channels, $W$ the window size and $N_{parallel\_adc}$ the number of parallel data sent by the ADCs.
		
		
		\paragraph*{Frequency limits}
		Another limitation to consider during the design is the frequency. 
		At the maximum sampling frequency of 4 GS/s, the ADC outputs packets of 8 samples at a time, reducing the frequency on the FPGA side to 500MHz.
		To be able to work at this frequency, the DSP blocks need to use registers at each calculation stage\footcite[][p. 63]{zynq_switching_characteristics}.
		The FIFO blocks used in the delayers (see high level implementation \ref{ss:high_level_implementation}) can also work at 500MHz\footcite[][p. 61]{zynq_switching_characteristics}. 
		At the maximum data input frequency, no particular frequency limits are hit. 
		However, during implementation, the signal propagation times can be too high and slow down the system or corrupt data. 
		Plenty of registering has been added and ease the timing constraints to avoid corruption.
		Nonetheless, the implementation of this project has kept the PL frequency at 250MHz in order to reduce timing limits as the design quickly increases the used surface area.
		
		
		% what is the maximum input channel number in the end?
		\paragraph*{}
		In the end, after all the timings and hardware limitations, the maximum count of input signals can be determined by the equation:
		
		\begin{equation}
			\frac{N_c \cdot (N_c - 1)}{2} \cdot W \cdot N_p + N_p \cdot N_c <= 4272
		\end{equation} 
		
		with $N_c$ the number of input channels, $W$ the width of the correlation window, and $N_p$ the number of parallel data streams from each ADC.
		$N_p$ can be defined as: $ N_p = \frac{f_{ADC}}{f_{PL}} <= 8$.
		
		\paragraph*{}
		For this project, the following configurations, chosen for their simplicity, have been tested: 
		\begin{itemize}
			\item 2 input channels, from 500MHz to 2GHz sampling frequency, from 62.5MHz to 250MHz PL frequency
			\item 3 input channels, 1GHz sampling frequency, 125MHz PL frequency
		\end{itemize}
		These configurations are created from the same source code, allowing for more flexibility in the solution's scaling.
		
		% for later 
		\paragraph*{}
		Another limitation of this project is the exporting of the data. 
		Doing the accumulation directly after the ADC output reduces the amount of data stored significantly. 
		Nonetheless, at high sampling frequencies, if the size of the batches (the number of samples to sum) is too small, the data rate can outgrow the export possibilities of the system.
		
		As the system can generate multiple Gb of data each minute, we decided to export all the data out of the card to limit the wear of the onboard flash storage. 
		For 2 channels at 1GHz for example:
		\begin{equation}
			datarate = ResultBitSize \frac{FPGA_{freq}}{batch_{size}} N_{dsp} = 48 \cdot \frac{125'000'000}{2^17} \cdot 832 = 38.1 Mb/s
		\end{equation}
		The data is exported to an external server via Gigabit ethernet. 
	
	
			
			

