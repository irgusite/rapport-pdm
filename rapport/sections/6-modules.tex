\newpage
\section{Modules}
	% General separation
	This section will present the different modules designed for this project.
	Starting with the accumulator modules and delayer, their design considerations will be discussed.
	Then, multiple modules done for debugging and testing purpose will be described.
	And finally, the top level module, simplifying the accumulators implementation is described.
	
	\input{sections/6_1-sigma_accums}
		
		
		\newpage
	\subsection{Correlation accumulators}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.2\linewidth]{figs/block_correlation}
			\caption{Correlation accumulator Vivado module}
			\label{fig:blockcorrelation}
		\end{figure}
	
		The correlation accumulator is the module responsible for the multiplication and accumulation of two signals. 
		The data is sent to the module using AXI stream, and the results are the read via the AXI bus.
		As a reminder, the equation to solve is :
		\begin{equation}
			\rho(d) = \frac{1}{n \sigma_x \sigma_y} \sum^w_{i=0}{(x_i \cdot y_{i+d})} - \langle x \rangle \langle y \rangle
		\end{equation}
		This module integrates the $\sum^w_{i=0}{(x_i \cdot y_{i+d})}$ equation in hardware.
		
		\subsubsection{Architecture}
			\begin{figure}[H]
				\centering
				\includegraphics[width=\linewidth]{figs/generated/Correlation_accum-Overview}
				\caption{General architecture of the correlation accumulator module. }
				\label{fig:correlationaccum-overview}
			\end{figure}
			The architecture of this module is very similar to the sigma accumulator. 
			Two major points are nonetheless different: the fact that we want multiple values of d (from $y_{i+d}$) means the module needs a memory and a lot more accumulators.
			As d can be equals to up to a hundred, the module needs to store a history of this size.
			As seen in figure \ref{fig:correlationaccum-overview}, the signals first get stored in storage modules.
			Then every value stored gets sent into an accumulator, one for each value of d (0 to 99) times one for each parallel data (8).
			After the accumulation, a multiplexer allows the AXI MM Slave module to serve the values to the software.
			
%			{signal: [
%	{name: 'clk', wave: 'P........|......'},
%	{name: 'sync', wave: '01.......|......'},
%	{name: "i_counter_sigma", wave :'2.2222222|222222', data:['0', '1', '2', '3', '4', '5', '6', '7', 'MAX','0','1','2','3','4']},
%	{name: "i_batch_count", wave :'2....2...|.....2', data:['0', '1', '2', '3', '4', '5', '6', '7', '8']},
%	{name: "i_sload(0)", wave: '010......|..10..'},
%	{name: "sload_reg", wave: '0.10.....|...10.'},
%	{name: "axis", wave :'x222222222|22222', data:['1','2','3','4','5','6','7','8','9']},
%	{name: "i_reg_a[0]", wave :'2.22222222|22222', data:['0', '1','2','3','4','5','6','7','8']},
%	{name: "a_reg", wave :'2..2222222|22222', data:['0', '1','2','3','4','5','6','7']},
%	{name: "b_reg", wave :'2..2222222|22222', data:['0', '1','2','3','4','5','6','7']},
%	{name: "mult_reg", wave: '2...222222|22222', data:['0', '1','3','6','10','15', '21']},
%	{name: "adder_out", wave: '2....222222|2222', data:['0', '1','3','6','10','15', '21']},
%	{name: "old_result", wave: '2....222222|2222', data:['0', '1','3','6','10','15']},
%	{name: "reg_out", wave: 'x....2......|..2', data:['0', 'sum','','6','10','15', '21']},
%	],
%	
%	config: {hscale: 2}, 
%	head: {tick:0}
%}	
		\subsubsection{Storage}
			The storage sub-module has the task to store enough history to be able to provide any value of $y_{i+d}$ with $ 0 <= i <= 7$ and $ 0 <= d <= w-1$ where w is the correlation window width.  
			Thus, having a window width of $d = 100$ means that a minimum of 100 values of Y needs to be stored at any given time, in order to accumulate every iteration possible.
			To do that, an array of $14 \times 8$ values is created. 
			To optimize the memory allocation, each value is cut to the 12 valid bits, and then stored in the order of sampling.
			This order is as follows : element 0 in the array is the most recent one, and element 111 in the array is the oldest one. 
			
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.5\linewidth]{figs/generated/Correlation_accum-Storage-Y}
				\caption{Storage of the channel A data. The AXI stream data is first split into the individual samples and stored into the 8 first array slots. The previous data is copied at the same time 8 array further. The numbers in the storage block represent the order of the data, with 0 being the oldest data and 111 the most recent one. The array numbers are not represented for clarity, but are inverse to the data order (0 for the most recent and 111 for the oldest)}
				\label{fig:correlationaccum-storage-y}
			\end{figure}

			This function is done by one process in the VHDL code. 
			This one is synced on the \mintinline{vhdl}{adc_clk}. 
			Two loops copy the actual values 8 steps later in the array and input the 8 new values from the \mintinline{vhdl}{s_axis_a_tdata} channel to the array.
			As shown in figure \ref{fig:correlationaccum-storage-y} the data must be put in reverse order of arrival as the sample 0 is the oldest one and the sample 7 is the most recent.
			Thus, the sample 0 goes into array 7 and the sample 7 in the array 0.
			Therefore, the loop (lines 199-201) uses \mintinline{vhdl}{samples_per_adc - (I + 1)} to invert the order of the data stream in memory.
			To select the 12 lower bits, the AXI stream data is truncated in the loop using \mintinline{vhdl}{((16 * (I + 1)) - 1 downto (16 * I) + 4)} as selector. 
			This returns the bits 15-4, 31-20 etc.
			
			\inputminted[firstline=193, lastline=196]{vhdl}{code/HW/Correlation_accumulators.vhd}
			\inputminted[firstline=199, lastline=201]{vhdl}{code/HW/Correlation_accumulators.vhd}
			
			
			\paragraph*{}
			To correct the delay created by storing Y, the signal X is also stored in a register, but without the whole history. 
			The sub-module simply stores the received batch of X into an array of eight twelve-bit values (figure \ref{fig:correlationaccum-storage-of-x}).
			
			\begin{figure}
				\centering
				\includegraphics[width=0.5\linewidth]{figs/generated/Correlation_accum-Storage-of-X}
				\caption{Storage of the channel B data. The AXI stream data is first split into the individual samples and stored into the array.}
				\label{fig:correlationaccum-storage-of-x}
			\end{figure}
			
			\inputminted[firstline=220, lastline=222]{vhdl}{code/HW/Correlation_accumulators.vhd}
			
		\subsubsection{Accumulator}
			As for the sigma accumulators, the accumulators are generated at synthesis time to have the right amount for the configuration.
			For the correlation, the logic behind are two nested loops. 
			The first loops from 0 to \mintinline{vhdl}{window_size} (shown in figure \ref{fig:correlationaccum-overview}).
			Inside this first loop, a second one loops from 0 to \mintinline{vhdl}{samples_per_batch - 1} in order to process the samples received in parallel.
			This accumulator is very similar to the square accumulator from the Sigma accumulators module  (figure \ref{fig:correlationaccum-accumulator}). 
			However instead of taking twice the same value, now the second ADC channel is is multiplied to the first one.
			After the multiplication, the value is accumulated to the previous ones until the reset signal stores the final value and resets the accumulator to 0.
			As this module is behaving like the squared accumulator from the sigma accumulators, the timing diagram is very similar (\ref{fig:corrtiming}).
			
			
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.7\linewidth]{figs/generated/Correlation_accum-Accumulator}
				\caption{Accumulator for the correlation. Here are show 2 accumulators from the total \mintinline{vhdl}{samples_per_batch}. Each accumulator takes the next sample value for the same correlation (same d value).}
				\label{fig:correlationaccum-accumulator}
			\end{figure}
		
			\begin{figure}[H]
				\centering
				\includegraphics[width=0.7\linewidth]{figs/corr_timing}
				\caption{Timing diagram of one correlation accumulator.}
				\label{fig:corrtiming}
			\end{figure}
			
		
		\subsubsection{Counter}
			The counter is the same as the one from the sigma accumulator module.
			It counts the samples and batches of samples to keep track of the data.
			Like the other counters, it is synchronized via the sync signal.
			
		
		\subsubsection{AXI slave registers}
			The results of the accumulators are available during the time it takes to accumulate a batch.
			During this time, the values can be read using the AXI Slave module.
			This module also allows to change the batch size and to read the size of the correlation window.
			The table \ref{table:corr_register_map} shows the available registers.
			\begin{table}[H]
				\begin{tabular}{|c|c|c|}
					\hline
					Address & Read (64 bits) & Write (64 bits) \\
					\hline
					0 & Batch size & Batch size \\
					\hline
					1 & Correlation size & -- \\
					\hline
					2-the end & Correlation accumulators results & -- \\
					\hline
				\end{tabular}
				\caption{Register map of correlation accumulators}
				\label{table:corr_register_map}
			\end{table}
		The results register gives the results in the following order: the results 2 to 9 for d = 0, 10 to 17 for d = 1 and so on.
		Inside a block, the results are in the same order as the Sigma accumulators results, which means, index 0 is the most recent sample and index 7 is the oldest.

	\subsection{Delayers}
	
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.3\linewidth]{figs/block_adc_delayer}
			\caption{Delayer Vivado module}
			\label{fig:blockadcdelayer}
		\end{figure}
		To be able to calculate the correlation outside of the 0-100 window generated by the accumulators and correct the signal propagation time through different cable length, the samples can be delayed. 
		This modules takes an AXI stream as input, and outputs the same AXI stream delayed. 
		It takes its configuration via AXI bus.
		This allows to modify the equation \ref{equ_correlation_split} to
		\begin{equation}
			\rho(d) = \frac{1}{n \sigma_x \sigma_y} \sum^w_{i=0}{(x_i \cdot y_{i+d-delayX+delayY})} - \langle x \rangle \langle y \rangle 
		\end{equation} 
		To add this delay, the ADC\_delayer module stores temporarily the samples in a FIFO and outputs them with a delay compared to the other channels. 
		
		\subsubsection{Architecture}
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/generated/Delayers-Block-design}
			\caption{Delayer module block design}
			\label{fig:delayers-block-design}
		\end{figure}
	
		As shown in figure \ref{fig:delayers-block-design} the delayer is made from a FIFO, a finite state machine to control it and an AXI Slave interface to configure it and get status information.
		
		To control the FIFO, multiple parameters must be taken into account\footcite{xilinx_fifo_generator_datasheet}: 
		\begin{itemize}
			\item There is latency between the input and output
			\item The reset takes multiple clock cycles
			\item The FIFO needs 2 clock cycles to output the first element.
		\end{itemize}
		Therefore, in order to control at best the FIFO and not loose any data, the following finite state machine has been designed:
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.6\linewidth]{figs/generated/Delayers-FSM}
			\caption{Finite State machine of the delayer module}
			\label{fig:delayers-fsm}
		\end{figure}
	
		Starting at the top, during the reset phase (which occurs at hardware reset or soft reset via an AXI register) the system goes to the \mintinline{vhdl}{INIT} state. 
		This state waits for the sync signal to be propagated to all the modules. 
		The sync is not necessary for the delayers, but having them synchronized helps to keep everything together.
		When sync goes to 1, the state machine goes into the \mintinline{vhdl}{RESET} state (see figure \ref{fig:delaytiming}). 
		This resets the signals and sets the FIFO reset signal to '1' during 3 clock cycles. 
		Once the three are done, the state machine goes to the \mintinline{vhdl}{READY} state.
		This one waits for all the reset busy lines to be back to 0, indicating that the FIFO completed the reset cycle.
		The next state is the \mintinline{vhdl}{WAIT_NOT_EMPTY}, which puts the \mintinline{vhdl}{write_enable} line high to start the data input into the fifo, and waits until the \mintinline{vhdl}{fifo_empty} signal goes low.
		This ensures that the FIFO is in a readable state, as the empty signal can be set to trigger with 2 or 3 elements in memory depending on configuration.
		After this, the \mintinline{vhdl}{WAIT_DELAY} state waits for the configured amount of clock cycles to delay the data.
		Once they are done, the \mintinline{vhdl}{read_enable} signal is set to 1, which outputs the data to the \mintinline{vhdl}{s_axis_out_a} bus.
		The \mintinline{vhdl}{IDLE} state is then maintained until a reset occurs, which restarts all this procedure. 

		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/delay_timing}
			\caption{Timing diagram of the delay module.}
			\label{fig:delaytiming}
		\end{figure}
		

%		{signal: [
%			{name: 'clk', wave: 'P...............'},
%			{name: 'sync', wave: '01..............'},
%			{name: "i_fsm_counter", wave :'2.222....2222...', data:['0', '1', '2', '0', '1', '2', '3', '0',]},
%			{name: "FSM", wave: "22..2..2.2..2...", data:['INIT', 'RESET', 'READY', 'WAIT_NOT_EMPTY', 'WAIT_DELAY', 'IDLE',]},
%			{name: "empty", wave: "1.......0......."},
%			{name: "wr_en", wave: "0......1........"},
%			{name: "rd_en", wave: "0...........1..."},
%			{name: "rd_rst_busy", wave: "01....0........."},
%			{name: "wr_rst_busy", wave: "01....0........."},
%			{name: "AXI Stream in", wave: "x.....2222222222", data:['0', '1', '2', '3', '4', '5', '6', '7','8','9',]},
%			{name: "AXI Stream out", wave: "x.............22", data:['2', '3', '4', '5', '6', '7','8','9',]},
%			],
%			
%			config: {hscale: 2}, 
%			head: {tick:0}
%		}
		
		As before, the input and output sizes are defined by the number of parallel data. 
		The AXI stream width is therefore N*16, with N the number of samples per ADC batch. 
		\inputminted[firstline=35, lastline=35]{vhdl}{code/HW/ADC_Delayer.vhd}
		The same width is applied to the data inputs and outputs of the FIFO. 
		Concerning the depth of the FIFO, it has been chosen to be 2048, in order to allow a correction of up to 512ns at 4GHz.
		The \mintinline{vhdl}{fifo_depth} and the \mintinline{vhdl}{samples_per_adc} are configurable via a generic.
		
		\inputminted[firstline=29, lastline=32]{vhdl}{code/HW/ADC_Delayer.vhd}
		
		\subsubsection{AXI slave registers}
			The AXI Slaver interface allows to set the delay, reset the module and finally read the overall status.
			The following register map is used to configure the module at runtime:
			\begin{table}[H]
				\label{tab:delay_register_map}
				\begin{tabular}{|c|c|c|c|}
					\hline
					Register & Address & Read (64 bits) & Write (64 bits) \\
					\hline
					0 & 0x00 & Fifo delay (in clock cycles) & Fifo delay (in clock cycles) \\
					\hline
					1 & 0x08 & Status (see table below) & [0]: reset (active at 1) \\
					\hline
				\end{tabular}
				\caption{Register map of the delayer module}
			\end{table}
		\begin{table}[H]
			\label{tab:delay_status_register}
				\begin{tabular}{|c|c|}
				\hline
				Bit & Status \\
				\hline
				0 & Reset \\
				\hline
				1 & FIFO Overflow \\
				\hline
				2 & FIFO Underflow \\
				\hline
				3 & N/A \\
				\hline
				4 & N/A \\
				\hline
				5 & FIFO read reset busy \\
				\hline
				6 & FIFO write reset busy \\
				\hline
			\end{tabular}
			\caption{Status register values of the delayer}
		\end{table}
		
		\subsubsection{Configuring and running the module}
			To use the module, a certain order has to be respected for it to work as intended.
			First, the sync signal must be reset back to 0. Secondly, the wanted delay has to be set by accessing the register 0x00, and the requested clock cycles have also to be set. 
			Then, the register 0x08 must be accessed and the value 1 written, in order to soft-reset the delayer. 
			Finally, the sync signal must be set back to 1, which will start the delayer's finite state machine and apply the new delay.
		
		
%\newpage
	\subsection{Signal Capture}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.3\linewidth]{figs/block_signal_capture}
			\caption{Signal Capture in Vivado}
			\label{fig:blocksignalcapture}
		\end{figure}
		
		To help validating and debugging the design, and also to help seeing the signal in a real environment, a capture module has been developed. 
		\subsubsection{Architecture}
		This module stores a small batch of data in a FIFO to be read via AXI from the Linux operating system.
		It takes two AXI streams as data input and outputs the stored values via AXI memory mapped registers.
		To be able to be synchronized with the other systems, the module is comprised of a write finite state machine connected to a sample counter.
		This counter is synced using the same \mintinline{vhdl}{Sync} signal as the other modules.
		
		To output the data, the AXI module is reading from a register containing the most ancien value. 
		This register is updated by the read finite state machine.
		
		Finally, a multiplexer allows the AXI slave to read the values in 4 batches of 64 bits. 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/generated/Signal_capture-Block-diagram}
			\caption{Signal Capture Block diagram}
			\label{fig:signalcapture-block-diagram}
		\end{figure}
	
		The FIFO memory size was defined by first defining the width of it, being two channels of 128 bits, a total width of 256 bits, and then looking at the biggest implementation possible. 
		This appeared to be 65536 elements in depth.
		However, the BlockRAM FIFO has one element less accessible than defined, which would mean to miss it to do the final verification.
		Thus, with this depth defined, the capture size was then defines as 32768 elements ($2^{15}$).
	
		The read finite state machine (figure \ref{fig:signalcapture-read-fsm}) takes four steps to update the output register value.
		First, the finite state machine starts in IDLE mode, waiting for the \mintinline{vhdl}{next_fifo} signal to go up.
		To avoid any double reads given that the \mintinline{vhdl}{next_fifo} signal is updated on a slower clock, the finite state machine waits until it goes back to 0 in the \mintinline{vhdl}{WAIT_DEASSERT} state.
		When these steps are done, the \mintinline{vhdl}{FIFO_NEXT} state generates the pulse on the \mintinline{vhdl}{rd_enable} signal of the FIFO to output the next result.
		Afterwards, the state machine goes into the last state where \mintinline{vhdl}{rd_enable} signal is set to '0' and the output of the FIFO is copied into the output register.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.5\linewidth]{figs/generated/Signal_capture-Read-FSM}
			\caption{Signal Capture Read Finite State Machine}
			\label{fig:signalcapture-read-fsm}
		\end{figure}
	
		The write state machine (figure \ref{fig:signalcapture-write-fsm}) is responsible to enable the \mintinline{vhdl}{wr_enable} signal of the FIFO at the right time.
		It also resets the FIFO before the next capture to avoid having old data kept in it.
		First, the finite state machine starts in the \mintinline{vhdl}{IDLE} state. 
		This state is hold until the \mintinline{vhdl}{capture} signal is set to '1' via the AXI Slave interface.
		Then, the state machine switches to the \mintinline{vhdl}{RESET_FIFO} state which asserts the reset line of the FIFO for 3 clock cycles. 
		After that, the state machine waits for the sample counter to arrive at ()$maxValue - 2$) to start the capture (the $-2$ is to start the capture at 0, taking into account the different registers to propagate through).
		The \mintinline{vhdl}{FIFO_WR} state is then held for \mintinline{vhdl}{batch_size + 7} to capture more than a batch, as the last 2 elements are not readable from the FIFO.
		The 7 has been defined arbitrarily as $ 7 > 2 $ in order to have a little more samples captured as the bare minimum.
		Finally, when the capture is finished, the write finite state machine returns in \mintinline{vhdl}{IDLE} state.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/generated/Signal_capture-Write-FSM}
			\caption{Signal Capture Write Finite State Machine}
			\label{fig:signalcapture-write-fsm}
		\end{figure}
		
	
	\subsection{Signal injector}
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.3\linewidth]{figs/block_signal_injector}
			\caption{Signal injector in Vivado}
			\label{fig:blocksignalinjector}
		\end{figure}
		The ADC values, even using a precise voltage source, are not absolutely stable and predictable.
		To help during the design phase, the possibility to inject a simple value to test all the accumulators is helpful.
		The modules receives the configuration via AXI Bus and outputs the desired signal via the AXI Stream port.
		As explained later in the result section, this module has been used to validate the batch size, and synchronization between modules.
		
		\subsubsection{Architecture}
		This module is comprised of a multiplexer that can choose between the real signals and the constant values stored in a register.
		These values can be changed at runtime using the AXI bus and writing to the internal registers.
		The same bus is also used to choose between the real signal and the constants.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/generated/Signal_injector}
			\caption{}
			\label{fig:signalinjector}
		\end{figure}
	
		\subsubsection{AXI slave registers}
			The module contains 5 registers. 
			The first one sets the status of the injector, injecting a constant, or letting the signal through.
			The four other ones allow to set the values of the two 128 bit constants to inject on the two channels.
		\begin{table}
		
			\begin{tabular}{|c|c|c|c|}
				\hline
				Register & Address & Read (64 bits) & Write (64 bits) \\
				\hline
				0 & 0x00 & Constant injection status & Constant injection status \\
				\hline
				1 & 0x08 & - & LOW signal value channel A (4*16 bits) \\
				\hline
				2 & 0x10 & - & HIGH signal value channel A (4*16 bits) \\
				\hline
				3 & 0x18 & - & LOW signal value channel B (4*16 bits) \\
				\hline
				4 & 0x20 & - & HIGH signal value channel B (4*16 bits) \\
				\hline
			\end{tabular}
			\label{tab:injector_register_map}
			\caption{Register map of the signal injector}
		\end{table}
	
	\subsection{System control}
		The system control module allows to get information about the system.
		It provides hardware implementation date, the PL clock speed and general errors via the AXI bus and the on-board LEDs.
		\subsubsection{Architecture}
		To obtain these information, the module is made of multiple processes in order to process clock data, the implementation timestamp and general ADC status (figure \ref{fig:systemcontrol}).
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/generated/System_control}
			\caption{System control block diagram}
			\label{fig:systemcontrol}
		\end{figure}
		
		
		Starting with the clock informations, the idea here is to use the AXI clock as a reference (100MHz) to count the number of clock cycles made by the ADC clock during 100 AXI clock cycles. 
		This returns the frequency of the different accumulator modules, which is linked to the external PLL frequency by the equation: $ f_{PLL} = n_{channels} \cdot f_{PLADC} $.
		This allows to add the frequency information to the data structure exported from the board.
		
		
		The second part of this module is the implementation timestamp. 
		To recover this information, the module accesses the \mintinline{vhdl}{USR_ACCESSE2} register, which is configured in the bitstream with user modifiable information.
		To access this register, some configurations in the software Vivado have been made \footcite{xilinx_user_accesse}.
		First, a synthesis or implementation must be done.
		This allows the access to the Vivado menu "Tools"->"Edit device properties".
		In this window, the "Configuration" menu on the left has to be selected.
		This leads to the windows showed in figure \ref{fig:vivadouseraccesse2}.
		The field "User Access" is the one that configures the previous register. 
		Short data can be put as needed in this field to give for example a revision number.
		In this project, the option "TIMESTAMP" was used, which inputs the timestamp of the bitstream generation into this register.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/vivado_useraccesse2}
			\caption{Device configuration menu of the Vivado software. The value in the fiels "User Access" is modified to "TIMESTAMP"}
			\label{fig:vivadouseraccesse2}
		\end{figure}
		
		The last part of the system control module shows the general status of the card using the on-board LEDs.
		The four LEDs are showing from right to left respectively: OverVoltage on the inputs, internal PLL locked, ADC clock frequency and External PLL locked.
		This allows to quickly see if there is a problem with the ADC converters or the clocks. 
		To generate the blinking led for the clock, a counter is incremented until 250'000'000 (this allows to have a 1Hz blinking when the ADC is sampling at 2Hz).
		When the counter resets to 0, the state of the third LED is switched. 
		Thus, it's quick to see at which frequency the external PLL is set, as the blinking depends on : $f_{LED} = f_{ADC}/(n/4)$  with $f_{LED}$ the frequency of the LED, $f_{PLL}$ the frequency of the ADC sampling and  n the number of samples per batch.
		The two PLL locked LEDs are directly connected to the PLL locked signals. 
		Finally, the OverVoltage signal is a simple OR condition on all the OverVoltage signals from the ADC.	 
		
		\subsubsection{AXI slave registers}		
			The system control has two readable registers to recover the information (table \ref{tab:system_control_register_map}).
			The implementation timestamp has the following bit format\footcite{xilinx_user_accesse}: ddddd\_MMMM\_yyyyyy\_hhhhh\_mmmmmm\_ssssss.
			Where d is the day in the month (1 to 31), M the month (1 to 12), y the year from 0 to 63 (2000 to 2063), h,m and s are the hours minutes and seconds.
			The second register contains the clock counter result, returning the programmable logic frequency approximation in MHz.
			\begin{table}[H]
				\begin{tabular}{|c|c|c|c|}
					\hline
					Register & Address & Read (64 bits) & Write (64 bits) \\
					\hline
					0 & 0x00 & [31 downto 0] Implementation timestamp & -- \\
					\hline
					1 & 0x08 & [10 downto 0] ADC clock frequency & -- \\
					\hline
				\end{tabular}
				\caption{Register map of the delayer module}
				\label{tab:system_control_register_map}
			\end{table}
	
	\subsection{Correlator Top Level}
		To simplify the use of the accumulator modules, a top level module has been designed.
		This section explain the design choices for this module, but the implementation has not been finished at the time of writing this report.
		\subsubsection{Architecture}
		The idea of this Correlator top level module is to automatically generate the right amount of sigma accumulators and correlation accumulators according to the number of input channels.
		To do so, the module reuses the previous sigma and correlation accumulators modules and manages the connections to the right signals (see figure \ref{fig:correlatortoplevel-general-view}). 
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/generated/Correlator_top_level-General-view}
			\caption{Correlator top level block diagram}
			\label{fig:correlatortoplevel-general-view}
		\end{figure}
		As the modules use AXI as communication bus, a simple multiplexer is used to transmit the signal from the input of the top level to each individual module (see figure \ref{fig:correlatortoplevel-axi-view}).
		To simplify the multiplexing and as the only configuration of the accumulator modules is the batch size, this information is removed from the AXI registers of these modules and an added signal is used to allow the size to be configured from the top level module.
		This allows in the end to not need to implement a full AXI crossbar for a shared configuration.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/generated/Correlator_top_level-AXI-view}
			\caption{Correlator Top Level, view from the AXI read bus perspective}
			\label{fig:correlatortoplevel-axi-view}
		\end{figure}
		The generation of the right amount of accumulators is done using two intricated generate loops in the VHDL code.
		\begin{minted}{vhdl}
			GEN_CHANNELS : for channel in 0 to number_input_signals - 1 generate -- 8 sample in //
				Sigma  : entity work.Sigma_accumulators generic map(...) port map(...);
				
				GEN_CORRELATIONS : for second_channel in 0 to channel generate
				
					GEN_NOT_SAME: if channel /= second_channel generate
						Corr   : entity work.Correlation_accumulators generic map(...) port map(...);
					end generate GEN_NOT_SAME;
				
				end generate GEN_CORRELATIONS;
			end generate GEN_CHANNELS;
		\end{minted}
		The first loop, \mintinline{vhdl}{GEN_CHANNELS}, iterates from 0 to the number of channels, and generates one sigma accumulator module per channel. 
		Inside this loop, a second one goes from 0 to the actual value of the first loop (it will go from 0 to 0, then 0 to 1, 0 to 2 and so on).
		During this second loop, the correlation accumulators will be generated, but only if the index of the second loop is not equal to the one of the first loop.
		In the end, this allows to have the correlation only between different channels, and, therefore, no autocorrelation.
		
		At the time of writing this, the logic is already implemented, all that remains is to finish implementing the communication multiplexers. 
		
		
	
	
		