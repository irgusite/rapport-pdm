\newpage
\section{Software}
	\subsection{Petalinux}
		Petalinux is the operating system running on the SoC.
		It is a lightweight Linux operating system, optimized to run on the Xilinx SoCs.
		A tool library is provided by Xilinx to generate the OS from the hardware definitions made in Vivado.
		The full explanation on how to generate a new petalinux is available in appendix \ref{A:Petalinux}.
	
	\subsection{Clock Configuration}
		The ADC relies on an external clock source. 
		On the HTG-ZRF8 board, this source is an external PLL from Texas Instruments: the LMX2594.
		To simplify the configuration, which is done via SPI, Texas Instruments provides a tool to generate the configuration register values. 
		The use of this tool require some informations about the PLL and its source Clock.
		These are found in the board's documentation\footcite{htg-zrf8-documentation} in the table 2 (Main Clocks).
		A summary is available in figure \ref{fig:htgdoctableadc}.
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.7\linewidth]{figs/HTG_Doc_table_ADC}
			\caption{ADC source clock and PLL. The chip U68 is falsely documented and is in reality a LMX2594 instead of a LMX2592.}
			\label{fig:htgdoctableadc}
		\end{figure}
	
		There is an error in the figure \ref{fig:htgdoctableadc}, as the U68 chip is changed on the board and is instead a LMX2594. 
		The documentation given by HiTech Global is not fully up to date nor complete.
		Nevertheless, it indicates us that the frequency of the ADC source clock is 122.88 MHz.
		Once the source frequency is known, we can configure the TICS-Pro tool to generate the register configuration values. 
		\\
		\\
		In the software, the right chip must be selected: "File -> LMX2594". 
		Then, a known working register value file has to be loaded, or the values from figure 5.2 have to be used. 
		The value in the field "RFoutA" (blue filed in the figure \ref{fig:tics-pro1ghzconfig}) must be changed, in order to modify the output freqency. 
		Once all of this is done, the "Enter" key can be pressed.
		
		This will calculate the optimal configuration for the wanted frequency.
		The value of this field will likely change to the nearest possible value. 
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=\linewidth]{figs/TICS-pro_1GHZ_config}
			\caption{TICS-pro configuration tool. Configured here for 1GHz}
			\label{fig:tics-pro1ghzconfig}
		\end{figure}
		
		Now that the values are set, the hex register values have to be exported by going to "File" and "export hex register values". 
		The file created must be saved and, afterwards, uploaded to the SoC, using scp or equivalent.
		\\
		\\
		Once the file is on the board, the ticspro\_to\_gpio.cp program is used to upload the register values to the PLL.	
		This software was provided and used as is. 
		It uses a gpio port connected to the PLL in order to simulate an SPI connection. 
		To load the configuration, the code must be compiled and run with the following commands:
		\begin{minted}{bash}
			cd /home/etienne/gpu-sii/working_adc_2GHz
			make ticspro_to_gpio
			./ticspro_to_gpio 0xA0020000 ./HexRegisterValues2GHz.txt
		\end{minted}
		0xA0020000 is the address of the gpio connected to the PLL and the second argument is the file uploaded before.
		That way, the configuration of the PLL is completed.
		To check that everything went well, it should be verified on the board that the third LED from the right blinks, and that the second and the fourth LEDs are lit.
		
	\subsection{Data sending}
		Once the programmable logic is configured and the PLL is running, the data acquisition can start. 
		To do it and configure the different modules, a C++ class has been developed.
		This class, called RFSoC, does all the AXI interfacing with the modules and exposes multiple methods to configure the modules and retrieve the data.
		These are then used in the fifo\_recovery.cpp application.
		This one first configures the programmable logic modules, then starts the data acquisition and finally sends the results out to a client using the zeroMQ framework (see figure \ref{fig:softwareschema}).
				
		\begin{figure}[H]
			\centering
			\includegraphics[height=10cm]{figs/generated/Software_schema-high-level}
			\caption{Software schematic.}
			\label{fig:softwareschema}
		\end{figure}
	
		For the module configuration, the RFSOC class first instantiates the \mintinline{bash}{/dev/mem} connection and creates pointer to all the modules.
		Then, the delayers, the signal injector and batch sizes are configured.
		After that, the system starts the data acquisition.
		
		This is done by polling the batch number of the first sigma and waiting for it to be different from the last acquisition.
		Then, all the results are recovered from the accumulators, before reading the batch number again.
		If the batch number at the start of the acquisition is the same as the one at the end, then the data is coherent.
		If not, it means the batch has been updated while reading, and a new reading is started (see figure \ref{fig:softwareschema-dataupdate}).
		
		\begin{figure}[H]
			\centering
			\includegraphics[width=0.3\linewidth]{figs/generated/Software_schema-data_update}
			\caption{Data update logic.}
			\label{fig:softwareschema-dataupdate}
		\end{figure}
		
		After acquisition, the data is sent to the client in the form of multiple structures containing the data.
		
		To do it, three different structures are used, so that the configuration of the FPGA stays flexible.
		As the number of sigma accumulators and correlators can change, the result of each module is stored into a separate structure.
		These are called \mintinline{cpp}{strcut AccumulatorsSigma} and \mintinline{cpp}{struct AccumulatorsCorr}.
		\\
		\\
		Starting with \mintinline{cpp}{struct AccumulatorsSigma}, the structure contains a header to distinguish the type of structure received. 
		Then, the channel number associated with the packet is present, as well as the the batch number.
		These allow to reconstruct the data by matching the batch number between multiple packets and associating the channels together.
		Finally, the structure contains two 64 bit tables with the accumulated values for the channel.
		These are \mintinline{cpp}{NUM_BUSSES} long to hold the data from each parallel accumulator individually. 
		\inputminted[firstline=28,lastline=36]{cpp}{code/SW/squared_accumulators_data_structure.h}
		\\
		\\
		Then, for the \mintinline{cpp}{struct AccumulatorsCorr} structure, the same applies for the header, the batch number and the channels.
		Unlike the other structure, there are here two channels instead of one.
		Moreover, the differences lie on the data members \mintinline{cpp}{real_corr_size}, which indicate the size of the correlation window, and on the table \mintinline{cpp}{corr[CORR_SIZE]}, which contains the values of each accumulator.
		
		\inputminted[firstline=38,lastline=47]{cpp}{code/SW/squared_accumulators_data_structure.h}
		\\
		\\
		The next structure used is the \mintinline{cpp}{GeneralInfos}. 
		This one contains all the general information about the logic of the board.
		It is comprised of information about the sampling frequency, the delays applied to the input channels, the batch size, the number of data outputted in parallel from the ADCs, the total number of input channels (not implemented in hardware at the time of this report) and the timestamp of the implementation of the hardware.
		\inputminted[firstline=49,lastline=59]{cpp}{code/SW/squared_accumulators_data_structure.h}
		\\
		\\
		A final structure is used to send the content of the signal capture (\mintinline{cpp}{FifoX2}). 
		This one contains a header, the batch number and the content of the capture stored in two tables of 32768 elements, one for each channel capture.
		The data is stored on 16 bits, with only the 12 most significant bits containing ADC data. 
		The four lower bits must be discarded, as the ADCs may put random data on these bits.
		\inputminted[firstline=61,lastline=69]{cpp}{code/SW/squared_accumulators_data_structure.h}
		
		
	
	\subsection{Checking the data}
		\label{ss:checking_data}
		To validate the working of the system as a whole, a small program has been written to take the data sent by the SoC and compare it to software calculation.
		This software works with the signal capture module.
		First, the data acquisition is launched on the board, and the app saver\_text.c is used on a network connected computer to recover the results of the acquisition.
		The saver outputs multiple files:
		\begin{itemize}
			\item sigma.txt
			\item corr.txt
			\item fifo\_XXXX.txt (X represents the batch number)
		\end{itemize}
		These files are then run through the verification app by running \mintinline{bash}{./verif XXXX}, with XXXX the number of the signal capture batch.
		The application then passes through the fifo\_XXXX.txt file and does the same calculations as done on the hardware, namely: calculate the sum of the signal, the sum of the signal squared and the sum of two signal multiplied together.
		Then, the program searches the corresponding batches in the sigma.txt and corr.txt files. 
		Once found, they are compared to the computed values and the results are shown next side by side, as shown in the results (figure \ref{fig:verificatorapp}).
		
		As the values stored in the files are raw values, the system has to do some operations to obtain the real signed integer values.
		First, for the captured signal, each value is stored as 16 bits unsigned, and has to be converted to the signed 12 bits from the ADC.
		To do that, each one is firstly copied in a signed short, which brings back the sign, and then bit-shifted four bits to the right. 
		This allows to have the 12bit signed value from the ADC.
		
		Then, the correlation and sigma values are stored in 64 bits unsigned values. 
		In reality, only the 48 least significant bits are the output of the accumulators, the other bits being 0.
		To recover the signed values, the 48th bit is checked and, if it's 1, it means the value is negative.
		The latter is obtained doing a bit inverstion and masking it with 48 bits (\mintinline{cpp}{sigma_x[j] = ~sigma_x[j] & 0xFFFFFFFFFFFFULL;}).
		Then, 1 is added to the value, which is finally multiplied by -1.
		
		This verification is done for all the correlations and sigma accumulator values.
	
		\begin{minted}{cpp}
			sx[i % 8] += shifted;
			sy[i % 8] += shifted2;
			calculated_corr[i%8] += shifted*shifted2;
		\end{minted}
		
		In the end, the signal values are accumulated in software to generate the sigma values and correlation values.
		The validity of the final results can be checked by comparing them with the results obtains in the verification, as they are displayed side by side.
		The results are shown in the Results section of the report.
		
		