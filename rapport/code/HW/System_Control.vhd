----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11.11.2021 08:41:59
-- Design Name: 
-- Module Name: System_Control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;



entity System_Control is
    Port ( 
    --sync_n        : out std_logic;
    
    axi_clk_in    : in std_logic;
    adc_clk_in    : in std_logic;
    
    s_axi_awaddr  : in std_logic_vector(7 downto 0); -- Write address (optional)
    s_axi_awvalid : in std_logic; -- Write address valid (optional)
    s_axi_awready : out std_logic; -- Write address ready (optional)
    
    s_axi_wdata   : in std_logic_vector(63 downto 0); -- Write data (optional)
    s_axi_wlast   : in std_logic; -- Write last beat (optional)
    s_axi_wvalid  : in std_logic; -- Write valid (optional)
    s_axi_wready  : out std_logic; -- Write ready (optional)
    
    s_axi_bresp   : out std_logic_vector(1 downto 0); -- Write response (optional)
    s_axi_bvalid  : out std_logic; -- Write response valid (optional)
    s_axi_bready  : in std_logic; -- Write response ready (optional)
    
    s_axi_araddr  : in std_logic_vector(7 downto 0); -- Read address (optional)
    s_axi_arvalid : in std_logic; -- Read address valid (optional)
    s_axi_arready : out std_logic; -- Read address ready (optional)
    
    s_axi_rdata   : out std_logic_vector(63 downto 0); -- Read data (optional)
    s_axi_rvalid  : out std_logic; -- Read valid (optional)
    s_axi_rready  : in std_logic; -- Read ready (optional)
    
    s_axi_aclk    : in std_logic;
    s_axi_aresetn : in std_logic;
    
    LEDs          : out STD_LOGIC_VECTOR (3 downto 0)
    
    );
end System_Control;

architecture Behavioral of System_Control is

--signal i_clk_counter    : integer range 0 to 64;
signal i_clk_ratio      : integer range 0 to 64;
signal i_sync_n         : std_logic;

-- USR_ACCESS ports
signal CFGCLK: std_logic;
signal DATA: std_logic_vector(31 downto 0);
signal DATAVALID: std_logic;

  -- AXI MM signals
  signal axi_arready : STD_LOGIC;
  signal axi_rvalid  : STD_LOGIC;
  signal i_add_real  : unsigned (7 downto 0);
  
  signal i_clk_count_enable: STD_LOGIC;
  signal i_clk_counter: unsigned (9 downto 0);
  signal i_clk_counter_axi: unsigned (9 downto 0);
  signal i_clk_result: unsigned (9 downto 0);
  
  -- FSM for clock counter
  type FSM_CLK_CNT is (RESET, SAVE);
  signal i_fsm_clk : FSM_CLK_CNT;

begin

USR_ACCESSE2_inst : USR_ACCESSE2
   port map (
      CFGCLK => CFGCLK,       -- 1-bit output: Configuration Clock output
      DATA => DATA,           -- 32-bit output: Configuration Data output
      DATAVALID => DATAVALID  -- 1-bit output: Active high data valid output
   );
   
clock_count_enabler: process(s_axi_aclk)
begin
    if (rising_edge(s_axi_aclk)) then
        i_clk_counter_axi <= i_clk_counter_axi + 1;
        if (i_clk_counter_axi >= 100) then
            i_clk_counter_axi <= to_unsigned(0,10);
            i_clk_count_enable <= not i_clk_count_enable;
        end if;
    end if;
end process;

clock_count: process(adc_clk_in)
begin
    if (rising_edge(adc_clk_in)) then
        if (i_clk_count_enable = '1') then
            i_clk_counter <= i_clk_counter + 1;
            i_fsm_clk <= SAVE;
        else
            case i_fsm_clk is
            when RESET =>
                i_clk_counter <= to_unsigned(0,10);
            when SAVE =>
                i_clk_result <= i_clk_counter;
                i_fsm_clk <= RESET;
            end case;
        end if;
    end if;
end process clock_count;

  -- AXI MM Read Process
  axi_read : process (s_axi_aclk)
    variable v_add_shifted : unsigned(7 downto 0);
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        axi_arready <= '0';
        axi_rvalid  <= '0';
      elsif (s_axi_arvalid = '1' and axi_arready = '1') then
        axi_arready <= '1';
        axi_rvalid  <= '1';
        v_add_shifted := shift_right(unsigned(s_axi_araddr), 3);
        if v_add_shifted = 0 then
          s_axi_rdata(31 downto 0)  <= DATA;
          s_axi_rdata(63 downto 32) <= (others => '0');
        elsif v_add_shifted = 1 then
          s_axi_rdata(9 downto 0)  <= STD_LOGIC_VECTOR(i_clk_result);
          s_axi_rdata(63 downto 10) <= (others => '0');
        else
          s_axi_rdata <= x"DEADBEEFDEADBEEF";
        end if;

      else
        axi_rvalid  <= '0';
        axi_arready <= '1';
      end if;
    end if;
  end process;

  s_axi_rvalid  <= axi_rvalid;
  s_axi_arready <= axi_arready;


end Behavioral;
