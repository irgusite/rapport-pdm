----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Philipp Schuler
-- 
-- Create Date: 11/03/2021 04:05:46 PM
-- Design Name: 
-- Module Name: AXI_Fifo_Reader - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity AXI_Fifo_Reader is

  generic (
    axis_latency : INTEGER := 0
  );
  port (
    -- High speed interface for syncing
    adc_clk       : in STD_LOGIC; -- ADC side clock (250/500 MHz)
    s_axi_aclk    : in STD_LOGIC; -- AXI bus clock (100MHz)
    s_axi_aresetn : in STD_LOGIC; -- async reset for axi

    --sync_n        : in std_logic; -- synchronization signal for counting. Each module will add latency of 2 clock cycles. (has to replace resetn2 atm)
    -- TODO: implement the batch counter internally with clock and synced at 0 at reset.
    batch : in STD_LOGIC_VECTOR(31 downto 0); -- get the batch number from the nearest sigma_accumulator but should be calculated internally!!
    sync : in STD_LOGIC;
    
    s_axis_in_a_tdata  : in STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_in_a_tvalid : in STD_LOGIC;                      -- Transfer valid (required)
    s_axis_in_a_tready : out STD_LOGIC;                     -- Transfer ready (optional)

    s_axis_in_b_tdata  : in STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_in_b_tvalid : in STD_LOGIC;                      -- Transfer valid (required)
    s_axis_in_b_tready : out STD_LOGIC;                     -- Transfer ready (optional)
    
    fifo_data_out : out STD_LOGIC_VECTOR(255 downto 0); 

    -- AXI MM Slave port
    s_axi_awaddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Write address (optional)
    s_axi_awvalid : in STD_LOGIC;                    -- Write address valid (optional)
    s_axi_awready : out STD_LOGIC;                   -- Write address ready (optional)

    s_axi_wdata : in STD_LOGIC_VECTOR(63 downto 0); -- Write data (optional)
    --s_axi_wlast   : in std_logic;         -- Write last beat (optional)
    s_axi_wvalid : in STD_LOGIC;  -- Write valid (optional)
    s_axi_wready : out STD_LOGIC; -- Write ready (optional)

    s_axi_bresp  : out STD_LOGIC_VECTOR(1 downto 0); -- Write response (optional)
    s_axi_bvalid : out STD_LOGIC;                    -- Write response valid (optional)
    s_axi_bready : in STD_LOGIC;                     -- Write response ready (optional)

    s_axi_araddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Read address (optional)
    s_axi_arvalid : in STD_LOGIC;                    -- Read address valid (optional)
    s_axi_arready : out STD_LOGIC;                   -- Read address ready (optional)

    s_axi_rdata  : out STD_LOGIC_VECTOR(63 downto 0); -- Read data (optional)
    s_axi_rvalid : out STD_LOGIC;                     -- Read valid (optional)
    s_axi_rready : in STD_LOGIC;                      -- Read ready (optional)

    -- FIFO interface
    fifo_a_data : in STD_LOGIC_VECTOR (255 downto 0);
    fifo_a_read : out STD_LOGIC;
    fifo_a_w_en : out STD_LOGIC;
    fifo_a_full : in STD_LOGIC;
    fifo_a_rst  : out STD_LOGIC

  );
end AXI_Fifo_Reader;

architecture Behavioral of AXI_Fifo_Reader is

  attribute X_INTERFACE_INFO                  : STRING;
  attribute X_INTERFACE_PARAMETER             : STRING;
  
  attribute X_INTERFACE_PARAMETER of adc_clk : signal is "ASSOCIATED_BUSIF s_axis_in_a:s_axis_in_b:s_axis_out_a:s_axis_out_b, ASSOCIATED_RESET resetn";
  attribute X_INTERFACE_INFO of s_axi_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi_aclk CLK";
  
  --attribute X_INTERFACE_PARAMETER of adc_clk  : signal is "ASSOCIATED_BUSIF s_axis, ASSOCIATED_RESET resetn";
  
  attribute X_INTERFACE_INFO of s_axi_awaddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute X_INTERFACE_INFO of s_axi_wdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute X_INTERFACE_INFO of s_axi_wvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute X_INTERFACE_INFO of s_axi_wready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute X_INTERFACE_INFO of s_axi_bresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute X_INTERFACE_INFO of s_axi_bvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute X_INTERFACE_INFO of s_axi_bready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute X_INTERFACE_INFO of s_axi_araddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute X_INTERFACE_INFO of s_axi_rdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute X_INTERFACE_INFO of s_axi_rvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute X_INTERFACE_INFO of s_axi_rready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";

  constant max_count : INTEGER := 32768; -- 2^15 samples per batch + 7 to be able to empty the fifo
  constant fifo_size : INTEGER := 65535;

  constant fifo_empty_count : INTEGER := 7;

  signal sample_counter : INTEGER range 0 to 32780; -- allows to count where we are during capture
  signal synced_counter : INTEGER range 0 to 32768;

  -- Reset delaying
  signal resetn1 : STD_LOGIC;
  signal resetn2 : STD_LOGIC;

  signal batch_nb     : STD_LOGIC_VECTOR(31 downto 0);  -- We store the actual batch number here (taken from the sigma accumulator)
  signal output_reg_a : STD_LOGIC_VECTOR(255 downto 0); -- Register to store the fifo A data
  --signal output_reg_b     : std_logic_vector(127 downto 0);   -- Register to store the fifo B data

  signal axi_arready : STD_LOGIC; -- axi signals
  signal axi_rvalid  : STD_LOGIC;

  signal i_axi_aw_ready    : STD_LOGIC;
  signal i_axi_wresp_stall : STD_LOGIC;
  signal i_s_axi_bvalid    : STD_LOGIC;
  signal i_s_axi_awready   : STD_LOGIC;
  signal i_s_axi_wready    : STD_LOGIC;

  -- axi data and address pre buffers
  signal i_s_axi_add_pre_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_pre_buff : STD_LOGIC_VECTOR(63 downto 0);

  -- axi data and address synced buffers
  signal i_s_axi_add_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_buff : STD_LOGIC_VECTOR(63 downto 0);

  signal i_axi_valid_wraddr : STD_LOGIC;
  signal i_axi_valid_wdata  : STD_LOGIC;

  -- fifo common control signals
  signal i_fifo_wr_en  : STD_LOGIC;
  signal i_fifo_rst    : STD_LOGIC;
  signal capture       : STD_LOGIC; -- signal to start capturing a batch of adc
  signal capture_reset : STD_LOGIC; -- signal to reset the capture signal after the start of the capture

  signal next_fifo : STD_LOGIC;

  signal batch_to_capture : STD_LOGIC_VECTOR (31 downto 0);

  -- FSM for AXI Read 
  type FSM_AXI_R is (Idle, Address_valid, FIFO_UPD);
  signal i_fsm_axi_r : FSM_AXI_R;

  -- FSM for Fifo next element
  type FSM_FIFO_R is (IDLE, WAIT_DEASSERT, FIFO_NEXT, FIFO_READ);
  signal i_fsm_fifo_r : FSM_FIFO_R;

  -- FSM for fifo control
  type FSM_FIFO is(Idle, Reset_FIFO, WAIT_BATCH, FIFO_WR);
  signal i_fsm_fifo     : FSM_FIFO;
  signal fsm_fifo_count : INTEGER range 0 to 3;

begin

s_axis_in_a_tready <= '1';
s_axis_in_b_tready <= '1';

fifo_data_out(127 downto 0) <= s_axis_in_a_tdata;
fifo_data_out(255 downto 128) <= s_axis_in_b_tdata;

--  p_reset_latency : process (adc_clk, s_axi_aresetn)
--  begin
--    if (rising_edge(adc_clk)) then

--      resetn1 <= s_axi_aresetn;
--      resetn2 <= resetn1;

--    end if;
--  end process p_reset_latency;
  
p_counter : process (adc_clk)
  begin
    if (s_axi_aresetn = '0') then
      synced_counter <= 0;
    elsif (rising_edge(adc_clk)) then
      if (sync = '0') or (synced_counter >= max_count - 1) then
        synced_counter <= 0;
      else
        synced_counter <= synced_counter + 1;
      end if;
    end if;
  end process;

--  p_sample_counter : process (adc_clk, resetn2)
--  begin
--    if (rising_edge(adc_clk)) then

--      if (resetn2 = '0') then

--        synced_counter <= 0;

--      else

--        if (synced_counter < max_count - 1) then -- increment counter

--          synced_counter <= synced_counter + 1;

--        else

--          synced_counter <= 0;

--        end if;
--      end if;
--    end if;
--  end process p_sample_counter;

  fifo_a_w_en <= i_fifo_wr_en;
  fifo_a_rst  <= i_fifo_rst;

  p_fifo_read_control : process (s_axi_aclk, s_axi_aresetn)
  begin
    if (rising_edge(s_axi_aclk)) then

      if s_axi_aresetn = '0' then

        i_fsm_fifo_r <= IDLE;

      else

        fifo_a_read <= '0';
        case i_fsm_fifo_r is

          when IDLE =>
            if (next_fifo = '1') then
              i_fsm_fifo_r <= WAIT_DEASSERT;
            end if;

          when WAIT_DEASSERT =>
            if (next_fifo = '0') then
              i_fsm_fifo_r <= FIFO_NEXT;
            end if;

          when FIFO_NEXT =>
            i_fsm_fifo_r <= FIFO_READ;
            fifo_a_read  <= '1';

          when FIFO_READ =>
            output_reg_a <= fifo_a_data;
            fifo_a_read  <= '0';
            i_fsm_fifo_r <= IDLE;

        end case;
      end if;
    end if;
  end process;

  p_fifo_control : process (adc_clk, s_axi_aresetn) -- high speed process (250/500MHz)
  begin
    if (rising_edge(adc_clk)) then

      if (s_axi_aresetn = '0') then

        capture_reset  <= '0';
        sample_counter <= 0;
        i_fsm_fifo     <= Idle;
        i_fifo_wr_en   <= '0';

      else
        case i_fsm_fifo is

          when Idle =>
            capture_reset  <= '0';
            fsm_fifo_count <= 0;
            sample_counter <= 0;
            i_fifo_wr_en   <= '0';

            if capture = '1' then
              i_fsm_fifo <= Reset_FIFO;
            elsif unsigned(batch) = unsigned(batch_to_capture) - 1 then
              i_fsm_fifo <= Reset_FIFO;
            end if;

          when Reset_FIFO => -- RESET FIFO during 3 clock cycles.
            i_fifo_rst     <= '1';
            capture_reset  <= '1'; -- set the capture clear flag to reset capture to 0;
            fsm_fifo_count <= fsm_fifo_count + 1;

            if fsm_fifo_count = 3 then
              i_fifo_rst <= '0';
              i_fsm_fifo <= WAIT_BATCH;
            end if;

          when WAIT_BATCH =>
            i_fifo_wr_en <= '0';

            if synced_counter = max_count - 2 and fifo_a_full = '0' then --need -2 to sync with sigma as it takes the input at -1 in reality atm
              i_fsm_fifo <= FIFO_WR;
            end if;

          when FIFO_WR =>
            i_fifo_wr_en   <= '1';
            sample_counter <= sample_counter + 1;

            if sample_counter = (max_count + fifo_empty_count) then
              i_fsm_fifo   <= Idle;
              i_fifo_wr_en <= '0';
            end if;

            if (sample_counter = 10) then -- copy the batch number at count 10, allows for stable value
              batch_nb <= std_logic_vector(unsigned(batch)+1);
            end if;

        end case;

      end if;
    end if;
  end process;

  axi_read : process (s_axi_aclk, s_axi_aresetn, capture_reset) -- Slow speed process (100MHz)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then -- RESET

        axi_arready <= '0';
        axi_rvalid  <= '0';

      else
        if (s_axi_arvalid = '1' and axi_arready = '1') then -- AXI READ
          axi_arready <= '1';
          axi_rvalid  <= '1';

          if shift_right(unsigned(s_axi_araddr), 3) = 0 then -- (0x0000) read batch number
            s_axi_rdata(31 downto 0)  <= batch_nb;
            s_axi_rdata(63 downto 32) <= (others => '0');

          elsif shift_right(unsigned(s_axi_araddr), 3) = 1 then -- (0x0008) read first half fifo value
            s_axi_rdata <= output_reg_a(63 downto 0);

          elsif shift_right(unsigned(s_axi_araddr), 3) = 2 then -- (0x0010) read second half fifo value and load next
            s_axi_rdata <= output_reg_a(127 downto 64);

          elsif shift_right(unsigned(s_axi_araddr), 3) = 3 then -- (0x0018) read first half fifo value
            s_axi_rdata <= output_reg_a(191 downto 128);

          elsif shift_right(unsigned(s_axi_araddr), 3) = 4 then -- (0x0020) read second half fifo value and load next
            s_axi_rdata <= output_reg_a(255 downto 192);

          elsif shift_right(unsigned(s_axi_araddr), 3) = 5 then -- (0x0028) read control signals
            s_axi_rdata    <= (others => '0');
            s_axi_rdata(0) <= capture;
            s_axi_rdata(1) <= next_fifo;

          else -- Other addresses return DEADBEEFDEADBEEF to AXI
            s_axi_rdata <= x"DEADBEEFDEADBEEF";

          end if;
        else -- Reset AXI signals after the read
          axi_rvalid  <= '0';
          axi_arready <= '1';

        end if;

      end if;
    end if;
  end process;

  s_axi_rvalid  <= axi_rvalid;
  s_axi_arready <= axi_arready;

  -- !  AXI WRITE PROCESSES

  i_axi_wresp_stall  <= i_s_axi_bvalid and not s_axi_bready;  -- is our response channel stalled?
  i_axi_valid_wraddr <= s_axi_awvalid or not i_s_axi_awready; -- signals that we have a valid write address (awvalid or a buffered address (not awready))
  i_axi_valid_wdata  <= s_axi_wvalid or not i_s_axi_wready;   -- signals that we have a valid write data (same as above)

  s_axi_bvalid  <= i_s_axi_bvalid;
  s_axi_awready <= i_s_axi_awready;
  s_axi_wready  <= i_s_axi_wready;

  -- Axi address write ready signal assessing
  axi_aw_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_awready <= '1'; -- ready to get an address

      elsif (i_axi_wresp_stall = '1') then       -- if the response buffer is stalled, we can get an address only if nothing in buffer
        i_s_axi_awready <= not i_axi_valid_wraddr; -- if valid_wraddr is set, we have something in buffer or on input  and cannot get new address

      elsif (i_axi_valid_wdata = '1') then -- if a valid data is present, we don't care, we want the associated address
        i_s_axi_awready <= '1';

      else
        i_s_axi_awready <= i_s_axi_awready and not s_axi_awvalid; -- stay ready until we have a valid address incomming

      end if;
    end if;
  end process;

  -- Axi write ready signal assessing
  axi_w_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_wready <= '1'; -- ready to get a data

      elsif (i_axi_wresp_stall = '1') then     -- if the response buffer is stalled, we can get a data only if nothing in buffer
        i_s_axi_wready <= not i_axi_valid_wdata; -- if valid_wdata is set, we have something in buffer or on input  and cannot get new data

      elsif (i_axi_valid_wraddr = '1') then -- if a valid address is present, we don't care, we want the associated data
        i_s_axi_wready <= '1';

      else
        i_s_axi_wready <= i_s_axi_wready and not s_axi_wvalid; -- stay ready until we have a valid data incomming

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write address
  axi_aw_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then -- pre buffer address when ready and valid
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_add_pre_buff <= s_axi_awaddr;

      end if;

      if (i_s_axi_awready = '0') then -- when not ready, we have something in buffer (address was here before data)
        i_s_axi_add_buff <= i_s_axi_add_pre_buff;

      else
        i_s_axi_add_buff <= s_axi_awaddr;

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write data
  axi_w_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_data_pre_buff <= s_axi_wdata;

      end if;

      if (i_s_axi_wready = '0') then -- when not ready, we have something in buffer (data was here before address)
        i_s_axi_data_buff <= i_s_axi_data_pre_buff;

      else
        i_s_axi_data_buff <= s_axi_wdata;

      end if;
    end if;
  end process;

  -- writing the data
  axi_w_write_data_to_regs : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        capture   <= '0';
        next_fifo <= '0';

      else
        next_fifo <= '0';
        if (i_axi_wresp_stall = '0' and i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
          case i_s_axi_add_buff is

            when "00000000" => -- 0x00
              batch_to_capture <= i_s_axi_data_buff(31 downto 0);

            when "00101000" => -- 0x28
              capture   <= i_s_axi_data_buff(0);
              next_fifo <= i_s_axi_data_buff(1);

            when others =>
          end case;

        end if;
        if (capture = '1' and capture_reset = '1') then -- Reset capture signal after capture started
          capture <= '0';
        end if;
      end if;
    end if;
  end process;

  -- response process

  s_axi_bresp <= "00"; -- always valid response for this module

  axi_bresp : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_bvalid <= '0';

      elsif (i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
        i_s_axi_bvalid <= '1';

      else
        i_s_axi_bvalid <= '0';

      end if;
    end if;
  end process;

end Behavioral;