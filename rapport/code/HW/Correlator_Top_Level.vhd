----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.11.2021 15:55:53
-- Design Name: 
-- Module Name: Correlator_Top_Level - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;

entity Correlator_Top_Level is
    Generic(
        number_input_signals: integer range 2 to 6 := 6;
        values_per_batch: integer range 1 to 8 := 8
    );
    Port ( 
        -- Clocking ADC
        adc_clk         : in std_logic;
        
        -- AXI Stream Slave ports, get data from first ADC output 
        s_axis_a_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_a_tvalid  : in  std_logic; 
        s_axis_a_tready  : out std_logic; 
        
        s_axis_b_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_b_tvalid  : in  std_logic; 
        s_axis_b_tready  : out std_logic; 
        
        s_axis_c_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_c_tvalid  : in  std_logic; 
        s_axis_c_tready  : out std_logic; 
        
        s_axis_d_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_d_tvalid  : in  std_logic; 
        s_axis_d_tready  : out std_logic; 
        
        s_axis_e_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_e_tvalid  : in  std_logic; 
        s_axis_e_tready  : out std_logic; 
        
        s_axis_f_tdata   : in  std_logic_vector(127 downto 0);
        s_axis_f_tvalid  : in  std_logic; 
        s_axis_f_tready  : out std_logic; 
        
        -- Output signals
        
        -- AXI MM Slave port
        s_axi_awaddr  : in std_logic_vector(20 downto 0); -- Write address (optional)
        s_axi_awvalid : in std_logic; -- Write address valid (optional)
        s_axi_awready : out std_logic; -- Write address ready (optional)
        
        s_axi_wdata   : in std_logic_vector(63 downto 0); -- Write data (optional)
        s_axi_wlast   : in std_logic; -- Write last beat (optional)
        s_axi_wvalid  : in std_logic; -- Write valid (optional)
        s_axi_wready  : out std_logic; -- Write ready (optional)
        
        s_axi_bresp   : out std_logic_vector(1 downto 0); -- Write response (optional)
        s_axi_bvalid  : out std_logic; -- Write response valid (optional)
        s_axi_bready  : in std_logic; -- Write response ready (optional)
        
        s_axi_araddr  : in std_logic_vector(20 downto 0); -- Read address (optional)
        s_axi_arvalid : in std_logic; -- Read address valid (optional)
        s_axi_arready : out std_logic; -- Read address ready (optional)
        
        s_axi_rdata   : out std_logic_vector(63 downto 0); -- Read data (optional)
        s_axi_rvalid  : out std_logic; -- Read valid (optional)
        s_axi_rready  : in std_logic; -- Read ready (optional)
        
        s_axi_aclk      : in std_logic;
        s_axi_aresetn   : in std_logic;
        
        
        -- Sync Signals
        sync            : in std_logic;
        resetn          : in std_logic
    );
end Correlator_Top_Level;

architecture Behavioral of Correlator_Top_Level is

constant INPUTSIZE : integer := 128;


  type array_input is array (6 - 1 downto 0) of STD_LOGIC_VECTOR (INPUTSIZE-1 downto 0);
  
  
  signal input_data : array_input;
  signal i_general_valid : std_logic;


begin

input_data(0) <= s_axis_a_tdata;
input_data(1) <= s_axis_b_tdata;
input_data(2) <= s_axis_c_tdata;
input_data(3) <= s_axis_d_tdata;
input_data(4) <= s_axis_e_tdata;
input_data(5) <= s_axis_f_tdata;

i_general_valid <= s_axis_a_tvalid and s_axis_b_tvalid and s_axis_c_tvalid and s_axis_d_tvalid and s_axis_e_tvalid and s_axis_f_tvalid;

  GEN_CHANNELS :
  for channel in 0 to number_input_signals - 1 generate -- 8 sample in //

    Sigma  : entity work.Sigma_accumulators generic map(batch_size=>50) port map(adc_clock => adc_clk, resetn => resetn, s_axis_tdata => input_data(channel), 
    s_axis_tvalid => i_general_valid,
    s_axi_awaddr => s_axi_awaddr, s_axi_araddr => s_axi_araddr, s_axi_aresetn => s_axi_aresetn, s_axi_aclk => s_axi_aclk, s_axi_arvalid => s_axi_arvalid,
    s_axi_awvalid => s_axi_awvalid, s_axi_wdata => s_axi_wdata, s_axi_wlast => s_axi_wlast, s_axi_wvalid => s_axi_wvalid, 
    s_axi_bready => s_axi_bready, s_axi_rready => s_axi_rready, sync => sync);
    
    GEN_CORRELATIONS : 
        for second_channel in 0 to channel generate
        
            GEN_NOT_SAME: if channel /= second_channel generate
                Corr   : entity work.Correlation_accumulators generic map(samples_per_batch=>50) port map(adc_clk => adc_clk, 
                s_axis_a_tdata => input_data(channel) , s_axis_b_tdata => input_data(second_channel), s_axis_a_tvalid => i_general_valid, 
                s_axis_b_tvalid => i_general_valid, sync => sync, s_axi_aresetn => s_axi_aresetn,
                s_axi_awaddr => s_axi_awaddr, s_axi_araddr => s_axi_araddr, resetn => resetn, s_axi_aclk => s_axi_aclk, s_axi_arvalid => s_axi_arvalid,
                s_axi_awvalid => s_axi_awvalid, s_axi_wdata => s_axi_wdata, s_axi_wlast => s_axi_wlast, s_axi_wvalid => s_axi_wvalid, 
                s_axi_bready => s_axi_bready, s_axi_rready => s_axi_rready);
            end generate GEN_NOT_SAME;
            
    end generate GEN_CORRELATIONS;

  end generate GEN_CHANNELS;

end Behavioral;
