----------------------------------------------------------------------------------
-- Company: ISDC
-- Engineer: Philipp Schuler
-- 
-- Create Date: 10/18/2021 05:26:46 PM
-- Design Name: 
-- Module Name: Sigma_accumulators - Behavioral
-- Project Name: 
-- Target Devices: htg-zrf8 
-- Tool Versions: Vivado 2020.2
-- Description: This modules does the accumulation of the signal and accumulation 
-- of the squared signal for the standard deviation calculation. The STD is then 
-- done on CPU.
-- 
-- Dependencies: - Sigma_acc (accumulator)
--               - Sigma_sacc (squared accumulator)
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 2.0.0 - Rewriting of the macc and acc parts to use submodules
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;
--use work.type_library.all;

entity Sigma_accumulators is
  generic (
    samples_per_adc : INTEGER := 8     -- number of samples outputed by the ADC at a time
  );
  port (

    -- AXI Stream Slave ports, get data from ADC output 
    s_axis_tdata  : in STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_tvalid : in STD_LOGIC;                      -- Transfer valid (required)
    s_axis_tready : out STD_LOGIC;                     -- Transfer ready (optional)

    -- AXI MM Slave port
    s_axi_awaddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Write address (optional)
    s_axi_awvalid : in STD_LOGIC;                    -- Write address valid (optional)
    s_axi_awready : out STD_LOGIC;                   -- Write address ready (optional)

    s_axi_wdata  : in STD_LOGIC_VECTOR(63 downto 0); -- Write data (optional)
    s_axi_wlast  : in STD_LOGIC;                     -- Write last beat (optional)
    s_axi_wvalid : in STD_LOGIC;                     -- Write valid (optional)
    s_axi_wready : out STD_LOGIC;                    -- Write ready (optional)

    s_axi_bresp  : out STD_LOGIC_VECTOR(1 downto 0); -- Write response (optional)
    s_axi_bvalid : out STD_LOGIC;                    -- Write response valid (optional)
    s_axi_bready : in STD_LOGIC;                     -- Write response ready (optional)

    s_axi_araddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Read address (optional)
    s_axi_arvalid : in STD_LOGIC;                    -- Read address valid (optional)
    s_axi_arready : out STD_LOGIC;                   -- Read address ready (optional)

    s_axi_rdata  : out STD_LOGIC_VECTOR(63 downto 0); -- Read data (optional)
    s_axi_rvalid : out STD_LOGIC;                     -- Read valid (optional)
    s_axi_rready : in STD_LOGIC;                      -- Read ready (optional)

    s_axi_aclk    : in STD_LOGIC; -- axi clock
    s_axi_aresetn : in STD_LOGIC; -- axi reset 

    -- Accumulator control inputs
    resetn : in STD_LOGIC; -- reset_n
    sync   : in STD_LOGIC;
    count  : out STD_LOGIC_VECTOR (31 downto 0); -- counts the number of samples received

    batch : out STD_LOGIC_VECTOR (31 downto 0); -- count at which batch of data we are (clk_cnt/nb_samples)

    adc_clock : in STD_LOGIC -- clock input

  );
end Sigma_accumulators;

architecture Behavioral of Sigma_accumulators is

  attribute X_INTERFACE_INFO      : STRING;
  attribute X_INTERFACE_PARAMETER : STRING;

  attribute X_INTERFACE_PARAMETER of adc_clock : signal is "ASSOCIATED_BUSIF s_axis, ASSOCIATED_RESET resetn";
  attribute X_INTERFACE_INFO of s_axi_aclk     : signal is "xilinx.com:signal:clock:1.0 s_axi_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_awaddr   : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute X_INTERFACE_INFO of s_axi_awready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute X_INTERFACE_INFO of s_axi_wdata    : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute X_INTERFACE_INFO of s_axi_wlast    : signal is "xilinx.com:interface:aximm:1.0 S_AXI WLAST";
  attribute X_INTERFACE_INFO of s_axi_wvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute X_INTERFACE_INFO of s_axi_wready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute X_INTERFACE_INFO of s_axi_bresp    : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute X_INTERFACE_INFO of s_axi_bvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute X_INTERFACE_INFO of s_axi_bready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute X_INTERFACE_INFO of s_axi_araddr   : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute X_INTERFACE_INFO of s_axi_arready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute X_INTERFACE_INFO of s_axi_rdata    : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute X_INTERFACE_INFO of s_axi_rvalid   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute X_INTERFACE_INFO of s_axi_rready   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";

  constant INPUTSIZE          : NATURAL := 12; -- Input size (12 bit ADC)
  constant OUTPUTSIZE         : NATURAL := 48; -- Output size (max DSP)
  constant default_batch_size : INTEGER := 131072;
  constant max_batch_size     : INTEGER := 16777216;

  -- Type declaration
  -- type array_inputs is array (samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR (13 downto 0);
  type array_signed is array (samples_per_adc - 1 downto 0) of signed (INPUTSIZE - 1 downto 0);
  type array_signed_output is array (samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR (OUTPUTSIZE - 1 downto 0);
  type array_signed_mult is array (samples_per_adc - 1 downto 0) of signed (2 * INPUTSIZE - 1 downto 0);
  type array_delay is array (samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR (INPUTSIZE - 1 downto 0);

  -- FSM

  -- Declare registers for intermediate values
  signal mean_output, mean2_output : array_signed_output;

  signal batch_cnt : unsigned (31 downto 0);
  -- signal input_signal              : STD_LOGIC_VECTOR(31 downto 0);
  signal counter : INTEGER range 0 to max_batch_size - 1; -- (1 << 24) -1

  -- AXI MM signals
  signal axi_arready : STD_LOGIC;
  signal axi_rvalid  : STD_LOGIC;

  -- AXI signals
  signal i_axi_aw_ready    : STD_LOGIC;
  signal i_axi_wresp_stall : STD_LOGIC;
  signal i_s_axi_bvalid    : STD_LOGIC;
  signal i_s_axi_awready   : STD_LOGIC;
  signal i_s_axi_wready    : STD_LOGIC;

  -- axi data and address pre buffers
  signal i_s_axi_add_pre_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_pre_buff : STD_LOGIC_VECTOR(63 downto 0);

  -- axi data and address synced buffers
  signal i_s_axi_add_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_buff : STD_LOGIC_VECTOR(63 downto 0);

  signal i_axi_valid_wraddr : STD_LOGIC;
  signal i_axi_valid_wdata  : STD_LOGIC;

  signal i_reg_chan_a : array_delay;
  signal i_sload      : STD_LOGIC_VECTOR(1 downto 0);

  -- Configuration signals (registers)
  signal count_max    : unsigned (23 downto 0)                := TO_UNSIGNED(default_batch_size, 24); --2^17 samples (8 values per sample)
  signal i_batch_size : INTEGER range 0 to max_batch_size - 1 := default_batch_size;

begin
  -- T_valid sync
  p_batch_counter : process (adc_clock)
  begin
    if rising_edge(adc_clock) then
      if sync = '0' then
        batch_cnt <= TO_UNSIGNED(0, 32);
      else
        if counter = TO_UNSIGNED(3, 20) then -- signal at count = 1
          batch_cnt <= batch_cnt + 1;
        end if;
      end if;
    end if;

  end process batch_counter;

  p_output_storage_and_sload : process (adc_clock) -- need to be adaptable
  begin
    if (rising_edge(adc_clock)) then

      if (resetn = '0') then
        i_sload <= (others => '1');
      else

        i_sload <= (others => '0');
        if (counter = 0) then
          i_sload(0) <= '1';

        elsif (counter = 1) then
            i_sload(1) <= '1';
        end if;
      end if;
    end if;
  end process output_storage_and_sload;

  p_counter : process (adc_clock)
  begin
    if (s_axi_aresetn = '0') then
      counter <= 0;
    elsif (rising_edge(adc_clock)) then
      if (sync = '0') or (counter >= i_batch_size - 1) then
        counter <= 0;
      else
        counter <= counter + 1;
      end if;
    end if;
  end process;

  b_storage : process (adc_clock) -- stores b channel for 1 clock cycle
  begin

    if (rising_edge(adc_clock)) then

      if (resetn = '0') then

        for I in samples_per_adc - 1 downto 0 loop
          i_reg_chan_a(I) <= (others => '0');
        end loop;

      else
        -- copy adc value to array(7 downto 0)
        for I in samples_per_adc - 1 downto 0 loop
          i_reg_chan_a(samples_per_adc - (I + 1)) <= s_axis_tdata((16 * (I + 1)) - 1 downto (16 * I) + 4);
        end loop;

      end if;
    end if;

  end process b_storage;
  
  GEN_PARALLEL :
  for K in 0 to samples_per_adc - 1 generate -- 8 sample in //

    ACC : entity work.Sigma_acc
      generic map(
        input_size  => INPUTSIZE,
        output_size => OUTPUTSIZE
      )
      port map(
        clk     => adc_clock,
        a       => i_reg_chan_a(K),
        sload   => i_sload(0),
        reg_out => mean_output(K)
      );

    SACC : entity work.Sigma_sacc
      generic map(
        input_size  => INPUTSIZE,
        output_size => OUTPUTSIZE
      )
      port map(
        clk     => adc_clock,
        a       => i_reg_chan_a(K),
        sload   => i_sload(1),
        reg_out => mean2_output(K)
      );

  end generate GEN_PARALLEL;

  -- AXI MM Read Process
  axi_read : process (s_axi_aclk)
    variable v_add_shifted : unsigned(7 downto 0);
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        axi_arready <= '0';
        axi_rvalid  <= '0';
      elsif (s_axi_arvalid = '1' and axi_arready = '1') then
        axi_arready <= '1';
        axi_rvalid  <= '1';
        v_add_shifted := shift_right(unsigned(s_axi_araddr), 3);
        if v_add_shifted = 0 then

          s_axi_rdata(24 downto 0) <= STD_LOGIC_VECTOR(TO_UNSIGNED(i_batch_size, 25));

        elsif v_add_shifted < 8 + 1 then
          s_axi_rdata(OUTPUTSIZE - 1 downto 0) <= STD_LOGIC_VECTOR(mean_output(to_integer(v_add_shifted - 1)));
          s_axi_rdata(63 downto OUTPUTSIZE)    <= (others => '0');
        elsif v_add_shifted < 16 + 1 then
          s_axi_rdata(OUTPUTSIZE - 1 downto 0) <= STD_LOGIC_VECTOR(mean2_output(to_integer(v_add_shifted - 8 - 1)));
          s_axi_rdata(63 downto OUTPUTSIZE)    <= (others => '0');
        else
          s_axi_rdata <= x"DEADBEEFDEADBEEF";
        end if;

      else
        axi_rvalid  <= '0';
        axi_arready <= '1';
      end if;
    end if;
  end process;

  s_axi_rvalid  <= axi_rvalid;
  s_axi_arready <= axi_arready;

  s_axis_tready <= '1';

  batch <= STD_LOGIC_VECTOR(batch_cnt);
  count <= STD_LOGIC_VECTOR(to_unsigned(counter, 32));

  -- !  AXI WRITE PROCESSES

  i_axi_wresp_stall  <= i_s_axi_bvalid and not s_axi_bready;  -- is our response channel stalled?
  i_axi_valid_wraddr <= s_axi_awvalid or not i_s_axi_awready; -- signals that we have a valid write address (awvalid or a buffered address (not awready))
  i_axi_valid_wdata  <= s_axi_wvalid or not i_s_axi_wready;   -- signals that we have a valid write data (same as above)

  s_axi_bvalid  <= i_s_axi_bvalid;
  s_axi_awready <= i_s_axi_awready;
  s_axi_wready  <= i_s_axi_wready;

  -- Axi address write ready signal assessing
  axi_aw_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_awready <= '1'; -- ready to get an address

      elsif (i_axi_wresp_stall = '1') then       -- if the response buffer is stalled, we can get an address only if nothing in buffer
        i_s_axi_awready <= not i_axi_valid_wraddr; -- if valid_wraddr is set, we have something in buffer or on input  and cannot get new address

      elsif (i_axi_valid_wdata = '1') then -- if a valid data is present, we don't care, we want the associated address
        i_s_axi_awready <= '1';

      else
        i_s_axi_awready <= i_s_axi_awready and not s_axi_awvalid; -- stay ready until we have a valid address incomming

      end if;
    end if;
  end process;

  -- Axi write ready signal assessing
  axi_w_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_wready <= '1'; -- ready to get a data

      elsif (i_axi_wresp_stall = '1') then     -- if the response buffer is stalled, we can get a data only if nothing in buffer
        i_s_axi_wready <= not i_axi_valid_wdata; -- if valid_wdata is set, we have something in buffer or on input  and cannot get new data

      elsif (i_axi_valid_wraddr = '1') then -- if a valid address is present, we don't care, we want the associated data
        i_s_axi_wready <= '1';

      else
        i_s_axi_wready <= i_s_axi_wready and not s_axi_wvalid; -- stay ready until we have a valid data incomming

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write address
  axi_aw_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then -- pre buffer address when ready and valid
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_add_pre_buff <= s_axi_awaddr;

      end if;

      if (i_s_axi_awready = '0') then -- when not ready, we have something in buffer (address was here before data)
        i_s_axi_add_buff <= i_s_axi_add_pre_buff;

      else
        i_s_axi_add_buff <= s_axi_awaddr;

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write data
  axi_w_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_data_pre_buff <= s_axi_wdata;

      end if;

      if (i_s_axi_wready = '0') then -- when not ready, we have something in buffer (data was here before address)
        i_s_axi_data_buff <= i_s_axi_data_pre_buff;

      else
        i_s_axi_data_buff <= s_axi_wdata;

      end if;
    end if;
  end process;

  -- writing the data
  axi_w_write_data_to_regs : process (s_axi_aclk, s_axi_aresetn)
  begin
    if (s_axi_aresetn = '0') then
      i_batch_size <= default_batch_size;
    else
      if (rising_edge(s_axi_aclk)) then

        if (i_axi_wresp_stall = '0' and i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
          case i_s_axi_add_buff is

            when "00000000" => -- 0x00
              i_batch_size <= to_integer(unsigned(i_s_axi_data_buff(24 downto 0)));
            when others =>
          end case;
        end if;
      end if;
    end if;
  end process;

  -- response process

  s_axi_bresp <= "00"; -- always valid response for this module

  axi_bresp : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_bvalid <= '0';

      elsif (i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
        i_s_axi_bvalid <= '1';

      else
        i_s_axi_bvalid <= '0';

      end if;
    end if;
  end process;

end Behavioral;