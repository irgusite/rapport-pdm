----------------------------------------------------------------------------------
-- Company: 
-- Engineer:  Philipp Schuler
-- 
-- Create Date: 22.11.2021 10:01:35
-- Design Name: 
-- Module Name: Signal_injector - Behavioral
-- Project Name: 
-- Target Devices: htg-zrf8
-- Tool Versions: Vivado 2020.2
-- Description: Allows to inject a constant signal insted of the ADC signals to 
-- check the general working of the system
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
entity Signal_injector is
  port (-- AXI Stream Slave ports, get data from ADC output 
    s_axis_in_a_tdata  : in STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_in_a_tvalid : in STD_LOGIC;                      -- Transfer valid (required)
    s_axis_in_a_tready : out STD_LOGIC;                     -- Transfer ready (optional)

    s_axis_in_b_tdata  : in STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_in_b_tvalid : in STD_LOGIC;                      -- Transfer valid (required)
    s_axis_in_b_tready : out STD_LOGIC;                     -- Transfer ready (optional)

    s_axis_out_a_tdata  : out STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_out_a_tvalid : out STD_LOGIC;                      -- Transfer valid (required)
    s_axis_out_a_tready : in STD_LOGIC;                       -- Transfer ready (optional)

    s_axis_out_b_tdata  : out STD_LOGIC_VECTOR(127 downto 0); -- Transfer Data (optional)
    s_axis_out_b_tvalid : out STD_LOGIC;                      -- Transfer valid (required)
    s_axis_out_b_tready : in STD_LOGIC;                       -- Transfer ready (optional)

    -- AXI MM Slave port
    s_axi_awaddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Write address (optional)
    s_axi_awvalid : in STD_LOGIC;                    -- Write address valid (optional)
    s_axi_awready : out STD_LOGIC;                   -- Write address ready (optional)

    s_axi_wdata  : in STD_LOGIC_VECTOR(63 downto 0); -- Write data (optional)
    s_axi_wlast  : in STD_LOGIC;                     -- Write last beat (optional)
    s_axi_wvalid : in STD_LOGIC;                     -- Write valid (optional)
    s_axi_wready : out STD_LOGIC;                    -- Write ready (optional)

    s_axi_bresp  : out STD_LOGIC_VECTOR(1 downto 0); -- Write response (optional)
    s_axi_bvalid : out STD_LOGIC;                    -- Write response valid (optional)
    s_axi_bready : in STD_LOGIC;                     -- Write response ready (optional)

    s_axi_araddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Read address (optional)
    s_axi_arvalid : in STD_LOGIC;                    -- Read address valid (optional)
    s_axi_arready : out STD_LOGIC;                   -- Read address ready (optional)

    s_axi_rdata  : out STD_LOGIC_VECTOR(63 downto 0); -- Read data (optional)
    s_axi_rvalid : out STD_LOGIC;                     -- Read valid (optional)
    s_axi_rready : in STD_LOGIC;                      -- Read ready (optional)

    s_axi_aclk    : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;

    -- Accumulator control inputs  
    adc_clock : in STD_LOGIC -- clock input

    -- debug           : in std_logic    -- debug signal to force 1 as input of the accums. 
  );
end Signal_injector;

architecture Behavioral of Signal_injector is

  attribute X_INTERFACE_INFO      : STRING;
  attribute X_INTERFACE_PARAMETER : STRING;

  attribute X_INTERFACE_PARAMETER of adc_clock : signal is "ASSOCIATED_BUSIF s_axis_in_a:s_axis_in_b:s_axis_out_a:s_axis_out_b, ASSOCIATED_RESET resetn";

  attribute X_INTERFACE_INFO of s_axi_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_awaddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute X_INTERFACE_INFO of s_axi_wdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute X_INTERFACE_INFO of s_axi_wlast   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WLAST";
  attribute X_INTERFACE_INFO of s_axi_wvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute X_INTERFACE_INFO of s_axi_wready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute X_INTERFACE_INFO of s_axi_bresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute X_INTERFACE_INFO of s_axi_bvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute X_INTERFACE_INFO of s_axi_bready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute X_INTERFACE_INFO of s_axi_araddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute X_INTERFACE_INFO of s_axi_rdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute X_INTERFACE_INFO of s_axi_rvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute X_INTERFACE_INFO of s_axi_rready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";

  signal i_constant_signal  : STD_LOGIC;
  signal i_constant_value_a : STD_LOGIC_VECTOR(127 downto 0);
  signal i_constant_value_b : STD_LOGIC_VECTOR(127 downto 0);
  signal i_axi_aw_ready     : STD_LOGIC;
  signal i_axi_wresp_stall  : STD_LOGIC;
  signal i_s_axi_bvalid     : STD_LOGIC;
  signal i_s_axi_awready    : STD_LOGIC;
  signal i_s_axi_wready     : STD_LOGIC;

  -- axi data and address pre buffers
  signal i_s_axi_add_pre_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_pre_buff : STD_LOGIC_VECTOR(63 downto 0);

  -- axi data and address synced buffers
  signal i_s_axi_add_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_buff : STD_LOGIC_VECTOR(63 downto 0);

  signal i_axi_valid_wraddr : STD_LOGIC;
  signal i_axi_valid_wdata  : STD_LOGIC;

  signal i_s_axi_arready : STD_LOGIC;
  signal i_s_axi_rvalid  : STD_LOGIC;

begin

s_axis_in_a_tready <= '1';
s_axis_in_b_tready <= '1';

  signal_selector : process (adc_clock)
  begin
    if (rising_edge(adc_clock)) then
      if (i_constant_signal = '1') then
        s_axis_out_a_tdata <= i_constant_value_a;
        s_axis_out_a_tvalid <= '1';
        s_axis_out_b_tdata <= i_constant_value_b;
        s_axis_out_b_tvalid <= '1';
      else
        s_axis_out_a_tdata <= s_axis_in_a_tdata;
        s_axis_out_a_tvalid <= s_axis_in_a_tvalid;
        s_axis_out_b_tdata <= s_axis_in_b_tdata;
        s_axis_out_b_tvalid <= s_axis_in_b_tvalid;
      end if;
    end if;
  end process;

  -- !  AXI WRITE PROCESSES

  i_axi_wresp_stall  <= i_s_axi_bvalid and not s_axi_bready;  -- is our response channel stalled?
  i_axi_valid_wraddr <= s_axi_awvalid or not i_s_axi_awready; -- signals that we have a valid write address (awvalid or a buffered address (not awready))
  i_axi_valid_wdata  <= s_axi_wvalid or not i_s_axi_wready;   -- signals that we have a valid write data (same as above)

  s_axi_bvalid  <= i_s_axi_bvalid;
  s_axi_awready <= i_s_axi_awready;
  s_axi_wready  <= i_s_axi_wready;

  -- Axi address write ready signal assessing
  axi_aw_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_awready <= '1'; -- ready to get an address

      elsif (i_axi_wresp_stall = '1') then       -- if the response buffer is stalled, we can get an address only if nothing in buffer
        i_s_axi_awready <= not i_axi_valid_wraddr; -- if valid_wraddr is set, we have something in buffer or on input  and cannot get new address

      elsif (i_axi_valid_wdata = '1') then -- if a valid data is present, we don't care, we want the associated address
        i_s_axi_awready <= '1';

      else
        i_s_axi_awready <= i_s_axi_awready and not s_axi_awvalid; -- stay ready until we have a valid address incomming

      end if;
    end if;
  end process;

  -- Axi write ready signal assessing
  axi_w_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_wready <= '1'; -- ready to get a data

      elsif (i_axi_wresp_stall = '1') then     -- if the response buffer is stalled, we can get a data only if nothing in buffer
        i_s_axi_wready <= not i_axi_valid_wdata; -- if valid_wdata is set, we have something in buffer or on input  and cannot get new data

      elsif (i_axi_valid_wraddr = '1') then -- if a valid address is present, we don't care, we want the associated data
        i_s_axi_wready <= '1';

      else
        i_s_axi_wready <= i_s_axi_wready and not s_axi_wvalid; -- stay ready until we have a valid data incomming

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write address
  axi_aw_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then -- pre buffer address when ready and valid
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_add_pre_buff <= s_axi_awaddr;

      end if;

      if (i_s_axi_awready = '0') then -- when not ready, we have something in buffer (address was here before data)
        i_s_axi_add_buff <= i_s_axi_add_pre_buff;

      else
        i_s_axi_add_buff <= s_axi_awaddr;

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write data
  axi_w_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_data_pre_buff <= s_axi_wdata;

      end if;

      if (i_s_axi_wready = '0') then -- when not ready, we have something in buffer (data was here before address)
        i_s_axi_data_buff <= i_s_axi_data_pre_buff;

      else
        i_s_axi_data_buff <= s_axi_wdata;

      end if;
    end if;
  end process;

  -- writing the data
  axi_w_write_data_to_regs : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_constant_signal  <= '0';
        i_constant_value_a <= x"00200020002000200020002000200020";
        i_constant_value_b <= x"00200020002000200020002000200020";

      else
        if (i_axi_wresp_stall = '0' and i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
          case i_s_axi_add_buff is

            when "00000000" => -- 0x00
              i_constant_signal <= i_s_axi_data_buff(0);

            when "00001000" => -- 0x08
              i_constant_value_a(63 downto 0) <= i_s_axi_data_buff;

            when "00010000" => -- 0x10
              i_constant_value_a(127 downto 64) <= i_s_axi_data_buff;

            when "00011000" => -- 0x18
              i_constant_value_b(63 downto 0) <= i_s_axi_data_buff;

            when "00100000" => -- 0x20
              i_constant_value_b(127 downto 64) <= i_s_axi_data_buff;

            when others =>
          end case;

        end if;
      end if;
    end if;
  end process;

  -- response process

  s_axi_bresp <= "00"; -- always valid response for this module

  axi_bresp : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_bvalid <= '0';

      elsif (i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
        i_s_axi_bvalid <= '1';

      else
        i_s_axi_bvalid <= '0';

      end if;
    end if;
  end process;

  axi_read : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_arready <= '0';
        i_s_axi_rvalid  <= '0';
      elsif (s_axi_arvalid = '1' and i_s_axi_arready = '1') then
        i_s_axi_arready <= '1';
        i_s_axi_rvalid  <= '1';
        s_axi_rdata     <= x"DEADBEEFDEADBEEF";

      else
        i_s_axi_rvalid  <= '0';
        i_s_axi_arready <= '1';
      end if;
    end if;
  end process;

  s_axi_rvalid  <= i_s_axi_rvalid;
  s_axi_arready <= i_s_axi_arready;

end Behavioral;