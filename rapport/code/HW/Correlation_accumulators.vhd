----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.11.2021 15:48:54
-- Design Name: 
-- Module Name: Correlation_accumulators - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

library work;

entity Correlation_accumulators is
  generic (
    samples_per_adc   : INTEGER := 8;
    input_array_size  : INTEGER := 14 -- Array for chan_a history 14 allows to have 104 correlations
  );
  port (
    -- Clocking ADC
    adc_clk : in STD_LOGIC;

    -- AXI Stream Slave ports, get data from first ADC output 
    s_axis_a_tdata  : in STD_LOGIC_VECTOR(127 downto 0);
    s_axis_a_tvalid : in STD_LOGIC;
    s_axis_a_tready : out STD_LOGIC;

    -- AXI Stream Slave ports, get data from first ADC output 
    s_axis_b_tdata  : in STD_LOGIC_VECTOR(127 downto 0);
    s_axis_b_tvalid : in STD_LOGIC;
    s_axis_b_tready : out STD_LOGIC;

    -- Output signals

    -- AXI MM Slave port
    s_axi_awaddr  : in STD_LOGIC_VECTOR(13 downto 0); -- Write address (optional)
    s_axi_awvalid : in STD_LOGIC;                     -- Write address valid (optional)
    s_axi_awready : out STD_LOGIC;                    -- Write address ready (optional)

    s_axi_wdata  : in STD_LOGIC_VECTOR(63 downto 0); -- Write data (optional)
    s_axi_wlast  : in STD_LOGIC;                     -- Write last beat (optional)
    s_axi_wvalid : in STD_LOGIC;                     -- Write valid (optional)
    s_axi_wready : out STD_LOGIC;                    -- Write ready (optional)

    s_axi_bresp  : out STD_LOGIC_VECTOR(1 downto 0); -- Write response (optional)
    s_axi_bvalid : out STD_LOGIC;                    -- Write response valid (optional)
    s_axi_bready : in STD_LOGIC;                     -- Write response ready (optional)

    s_axi_araddr  : in STD_LOGIC_VECTOR(13 downto 0); -- Read address (optional)
    s_axi_arvalid : in STD_LOGIC;                     -- Read address valid (optional)
    s_axi_arready : out STD_LOGIC;                    -- Read address ready (optional)

    s_axi_rdata  : out STD_LOGIC_VECTOR(63 downto 0); -- Read data (optional)
    s_axi_rvalid : out STD_LOGIC;                     -- Read valid (optional)
    s_axi_rready : in STD_LOGIC;                      -- Read ready (optional)

    s_axi_aclk    : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    resetn        : in STD_LOGIC;
    -- Sync Signals
    sync : in STD_LOGIC
  );
end Correlation_accumulators;

architecture Behavioral of Correlation_accumulators is

  attribute X_INTERFACE_PARAMETER            : STRING;
  attribute X_INTERFACE_PARAMETER of adc_clk : signal is "ASSOCIATED_BUSIF s_axis_a:s_axis_b, ASSOCIATED_RESET resetn";

  attribute X_INTERFACE_INFO                  : STRING;
  attribute X_INTERFACE_INFO of s_axi_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_awaddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute X_INTERFACE_INFO of s_axi_wdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute X_INTERFACE_INFO of s_axi_wlast   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WLAST";
  attribute X_INTERFACE_INFO of s_axi_wvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute X_INTERFACE_INFO of s_axi_wready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute X_INTERFACE_INFO of s_axi_bresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute X_INTERFACE_INFO of s_axi_bvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute X_INTERFACE_INFO of s_axi_bready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute X_INTERFACE_INFO of s_axi_araddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute X_INTERFACE_INFO of s_axi_rdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute X_INTERFACE_INFO of s_axi_rvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute X_INTERFACE_INFO of s_axi_rready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";

  constant correlation_size   : INTEGER := 100;
  constant INPUTSIZE          : NATURAL := 12; -- Input size (12 bit ADC)
  constant OUTPUTSIZE         : NATURAL := 48; -- Output size (max DSP)
  constant default_batch_size : INTEGER := 131072;
  constant max_batch_size     : INTEGER := 16777216;

  -- Type declaration
  type array_storage is array (input_array_size * samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR (INPUTSIZE - 1 downto 0);
  type array_delay is array (samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR (INPUTSIZE - 1 downto 0);
  type array_output is array (correlation_size * samples_per_adc - 1 downto 0) of STD_LOGIC_VECTOR(OUTPUTSIZE - 1 downto 0);

  signal i_reg_chan_a      : array_storage;
  signal i_reg_chan_a_temp : array_storage;
  signal i_reg_chan_b      : array_delay;
  signal i_sload           : STD_LOGIC_VECTOR(correlation_size - 1 downto 0);
  signal i_reg_out         : array_output;
  signal i_counter         : INTEGER range 0 to max_batch_size - 1;
  signal i_batch_cnt       : unsigned (31 downto 0);
  signal i_batch_size      : INTEGER range 0 to max_batch_size - 1 := default_batch_size;

  -- AXI MM signals
  signal axi_arready : STD_LOGIC;
  signal axi_rvalid  : STD_LOGIC;

  -- AXI signals
  signal i_axi_aw_ready    : STD_LOGIC;
  signal i_axi_wresp_stall : STD_LOGIC;
  signal i_s_axi_bvalid    : STD_LOGIC;
  signal i_s_axi_awready   : STD_LOGIC;
  signal i_s_axi_wready    : STD_LOGIC;

  -- axi data and address pre buffers
  signal i_s_axi_add_pre_buff  : STD_LOGIC_VECTOR(13 downto 0);
  signal i_s_axi_data_pre_buff : STD_LOGIC_VECTOR(63 downto 0);

  -- axi data and address synced buffers
  signal i_s_axi_add_buff  : STD_LOGIC_VECTOR(13 downto 0);
  signal i_s_axi_data_buff : STD_LOGIC_VECTOR(63 downto 0);

  signal i_axi_valid_wraddr : STD_LOGIC;
  signal i_axi_valid_wdata  : STD_LOGIC;

begin

  s_axis_a_tready <= '1';
  s_axis_b_tready <= '1';

  batch_count : process (adc_clk)
  begin
    if rising_edge(adc_clk) then
      if sync = '0' then
        i_batch_cnt <= TO_UNSIGNED(0, 32);
      else
        if i_counter = TO_INTEGER(TO_UNSIGNED(2, 25)) then -- output register signal is at count = 1 so we update 1 count later the batch number
          i_batch_cnt <= i_batch_cnt + 1;
        end if;
      end if;
    end if;

  end process batch_count;

  -- internal sample counter (synced via low sync signal with other modules)
  counter : process (adc_clk)
  begin
    if (rising_edge(adc_clk)) then

      if (sync = '0') then

        i_counter <= 0;

      elsif (i_counter >= i_batch_size - 1) then

        i_counter <= 0;

      else

        i_counter <= i_counter + 1;

      end if;
    end if;
  end process counter;

  a_storage : process (adc_clk) -- stores a channel for 14 clock cycles 
  begin
    if (rising_edge(adc_clk)) then

      if (resetn = '0') then
        for I in input_array_size * samples_per_adc - 1 downto 0 loop
          i_reg_chan_a(I) <= (others => '0');
        end loop;
      else

        -- copy sample 1 sample further
        for I in (((input_array_size - 1) * (samples_per_adc)) - 1) downto 0 loop
          i_reg_chan_a(I + samples_per_adc) <= i_reg_chan_a(I);
        end loop;

        -- copy adc value to array(7 downto 0)
        for I in samples_per_adc - 1 downto 0 loop
          i_reg_chan_a(samples_per_adc - (I + 1)) <= s_axis_a_tdata((16 * (I + 1)) - 1 downto (16 * I) + 4);
        end loop;

      end if;
    end if;
  end process a_storage;

  b_storage : process (adc_clk) -- stores b channel for 1 clock cycle
  begin

    if (rising_edge(adc_clk)) then

      if (resetn = '0') then

        for I in samples_per_adc - 1 downto 0 loop
          i_reg_chan_b(I) <= (others => '0');
        end loop;

      else
        -- copy adc value to array(7 downto 0)
        for I in samples_per_adc - 1 downto 0 loop
          i_reg_chan_b(samples_per_adc - (I + 1)) <= s_axis_b_tdata((16 * (I + 1)) - 1 downto (16 * I) + 4);
        end loop;

      end if;
    end if;

  end process b_storage;

  output_storage_and_sload : process (adc_clk) -- need to be adaptable
  begin
    if (rising_edge(adc_clk)) then

      if (resetn = '0') then
        i_sload <= (others => '1');

      else
        i_sload <= (others => '0');

        if (i_counter = 1) then -- to sync the beggining with sigma 
          i_sload <= (others => '1');
        end if;

      end if;
    end if;
  end process output_storage_and_sload;

  GEN_MACC :
  for J in 0 to correlation_size - 1 generate -- generate up to correlation size DSPs
    GEN_PARALLEL :
    for K in 0 to samples_per_adc - 1 generate -- 8 sample in //

      MACC : entity work.MACC_correlation
        generic map(
          input_size  => INPUTSIZE,
          output_size => OUTPUTSIZE
        )
        port map(
          clk     => adc_clk,
          a       => i_reg_chan_a(J + K),
          b       => i_reg_chan_b(K),
          sload   => i_sload(J),
          reg_out => i_reg_out((J * samples_per_adc) + K)
        );

    end generate GEN_PARALLEL;
  end generate GEN_MACC;

  axi_read : process (s_axi_aclk)
    variable v_add_shifted : unsigned(13 downto 0);
  begin
    if (rising_edge(s_axi_aclk)) then

      if (s_axi_aresetn = '0') then

        axi_arready <= '0';
        axi_rvalid  <= '0';

      elsif (s_axi_arvalid = '1' and axi_arready = '1') then

        axi_arready <= '1';
        axi_rvalid  <= '1';
        v_add_shifted := shift_right(unsigned(s_axi_araddr), 3);
        s_axi_rdata <= (others => '0');

        if v_add_shifted = 0 then

          s_axi_rdata(7 downto 0)   <= STD_LOGIC_VECTOR(TO_UNSIGNED(correlation_size, 8));
          s_axi_rdata(63 downto 32) <= STD_LOGIC_VECTOR(i_batch_cnt);

        elsif v_add_shifted = 1 then

          s_axi_rdata(24 downto 0) <= STD_LOGIC_VECTOR(TO_UNSIGNED(i_batch_size, 25));

        elsif v_add_shifted < (samples_per_adc * correlation_size) + 2 then

          s_axi_rdata(OUTPUTSIZE - 1 downto 0) <= STD_LOGIC_VECTOR(i_reg_out(to_integer(v_add_shifted - 2))); -- (axi_araddr >> 3)
          s_axi_rdata(63 downto OUTPUTSIZE)    <= (others => '0');

        else

          s_axi_rdata <= x"DEADBEEFDEADBEEF";

        end if;

      else

        axi_rvalid  <= '0';
        axi_arready <= '1';

      end if;
    end if;
  end process;

  s_axi_rvalid  <= axi_rvalid;
  s_axi_arready <= axi_arready;

  s_axis_a_tready <= '1';
  s_axis_b_tready <= '1';

  -- !  AXI WRITE PROCESSES

  i_axi_wresp_stall  <= i_s_axi_bvalid and not s_axi_bready;  -- is our response channel stalled?
  i_axi_valid_wraddr <= s_axi_awvalid or not i_s_axi_awready; -- signals that we have a valid write address (awvalid or a buffered address (not awready))
  i_axi_valid_wdata  <= s_axi_wvalid or not i_s_axi_wready;   -- signals that we have a valid write data (same as above)

  s_axi_bvalid  <= i_s_axi_bvalid;
  s_axi_awready <= i_s_axi_awready;
  s_axi_wready  <= i_s_axi_wready;

  -- Axi address write ready signal assessing
  axi_aw_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_awready <= '1'; -- ready to get an address

      elsif (i_axi_wresp_stall = '1') then       -- if the response buffer is stalled, we can get an address only if nothing in buffer
        i_s_axi_awready <= not i_axi_valid_wraddr; -- if valid_wraddr is set, we have something in buffer or on input  and cannot get new address

      elsif (i_axi_valid_wdata = '1') then -- if a valid data is present, we don't care, we want the associated address
        i_s_axi_awready <= '1';

      else
        i_s_axi_awready <= i_s_axi_awready and not s_axi_awvalid; -- stay ready until we have a valid address incomming

      end if;
    end if;
  end process;

  -- Axi write ready signal assessing
  axi_w_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_wready <= '1'; -- ready to get a data

      elsif (i_axi_wresp_stall = '1') then     -- if the response buffer is stalled, we can get a data only if nothing in buffer
        i_s_axi_wready <= not i_axi_valid_wdata; -- if valid_wdata is set, we have something in buffer or on input  and cannot get new data

      elsif (i_axi_valid_wraddr = '1') then -- if a valid address is present, we don't care, we want the associated data
        i_s_axi_wready <= '1';

      else
        i_s_axi_wready <= i_s_axi_wready and not s_axi_wvalid; -- stay ready until we have a valid data incomming

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write address
  axi_aw_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then -- pre buffer address when ready and valid
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_add_pre_buff <= s_axi_awaddr;

      end if;

      if (i_s_axi_awready = '0') then -- when not ready, we have something in buffer (address was here before data)
        i_s_axi_add_buff <= i_s_axi_add_pre_buff;

      else
        i_s_axi_add_buff <= s_axi_awaddr;

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write data
  axi_w_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_data_pre_buff <= s_axi_wdata;

      end if;

      if (i_s_axi_wready = '0') then -- when not ready, we have something in buffer (data was here before address)
        i_s_axi_data_buff <= i_s_axi_data_pre_buff;

      else
        i_s_axi_data_buff <= s_axi_wdata;

      end if;
    end if;
  end process;

  -- writing the data
  axi_w_write_data_to_regs : process (s_axi_aclk, s_axi_aresetn)
  begin
    if (s_axi_aresetn = '0') then
      i_batch_size <= default_batch_size;
    else
      if (rising_edge(s_axi_aclk)) then

        if (i_axi_wresp_stall = '0' and i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
          case i_s_axi_add_buff is

            when "00000000" => -- 0x00
              i_batch_size <= to_integer(unsigned(i_s_axi_data_buff(24 downto 0)));

              --            when "00001000" => -- 0x08
              --              i_soft_reset <= i_s_axi_data_buff(0);

            when others =>
          end case;
        end if;
      end if;
    end if;
  end process;

  -- response process

  s_axi_bresp <= "00"; -- always valid response for this module

  axi_bresp : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_bvalid <= '0';

      elsif (i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
        i_s_axi_bvalid <= '1';

      else
        i_s_axi_bvalid <= '0';

      end if;
    end if;
  end process;
end Behavioral;