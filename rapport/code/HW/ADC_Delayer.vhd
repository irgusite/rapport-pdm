----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Philipp Schuler
-- 
-- Create Date: 11/29/2021 03:47:45 PM
-- Design Name: 
-- Module Name: ADC_Delayer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: Vivado 2020.2
-- Description: 
-- 
-- Dependencies: XPM FIFO
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
library xpm;
use xpm.vcomponents.all;
library UNISIM;
use UNISIM.VComponents.all;

entity ADC_Delayer is
  generic (
    fifo_depth      : INTEGER := 2048;
    samples_per_adc : INTEGER := 8
  );
  port (
    -- AXI Stream Slave ports, get data from ADC output 
    s_axis_in_a_tdata  : in STD_LOGIC_VECTOR((samples_per_adc * 16) - 1 downto 0); -- Transfer Data
    s_axis_in_a_tvalid : in STD_LOGIC;                                             -- Transfer valid
    s_axis_in_a_tready : out STD_LOGIC;                                            -- Transfer ready

    s_axis_out_a_tdata  : out STD_LOGIC_VECTOR((samples_per_adc * 16) - 1 downto 0); -- Transfer Data
    s_axis_out_a_tvalid : out STD_LOGIC;                                             -- Transfer valid
    s_axis_out_a_tready : in STD_LOGIC;                                              -- Transfer ready

    -- AXI MM Slave port
    s_axi_awaddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Write address
    s_axi_awvalid : in STD_LOGIC;                    -- Write address valid
    s_axi_awready : out STD_LOGIC;                   -- Write address ready

    s_axi_wdata  : in STD_LOGIC_VECTOR(63 downto 0); -- Write data
    s_axi_wvalid : in STD_LOGIC;                     -- Write valid
    s_axi_wready : out STD_LOGIC;                    -- Write ready

    s_axi_bresp  : out STD_LOGIC_VECTOR(1 downto 0); -- Write response
    s_axi_bvalid : out STD_LOGIC;                    -- Write response valid
    s_axi_bready : in STD_LOGIC;                     -- Write response ready

    s_axi_araddr  : in STD_LOGIC_VECTOR(7 downto 0); -- Read address
    s_axi_arvalid : in STD_LOGIC;                    -- Read address valid
    s_axi_arready : out STD_LOGIC;                   -- Read address ready

    s_axi_rdata  : out STD_LOGIC_VECTOR(63 downto 0); -- Read data
    s_axi_rvalid : out STD_LOGIC;                     -- Read valid
    s_axi_rready : in STD_LOGIC;                      -- Read ready

    -- High speed interface for syncing
    adc_clk       : in STD_LOGIC; -- ADC side clock (250/500 MHz)
    sync          : in STD_LOGIC; -- general sync
    s_axi_aclk    : in STD_LOGIC; -- AXI bus clock (100MHz)
    s_axi_aresetn : in STD_LOGIC  -- async reset for axi
  );
end ADC_Delayer;

architecture Behavioral of ADC_Delayer is

  attribute X_INTERFACE_INFO      : STRING;
  attribute X_INTERFACE_PARAMETER : STRING;

  attribute X_INTERFACE_PARAMETER of adc_clk  : signal is "ASSOCIATED_BUSIF s_axis_in_a:s_axis_out_a";
  attribute X_INTERFACE_INFO of s_axi_aclk    : signal is "xilinx.com:signal:clock:1.0 s_axi_aclk CLK";
  attribute X_INTERFACE_INFO of s_axi_awaddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWADDR";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI AWREADY";
  attribute X_INTERFACE_INFO of s_axi_wdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI WDATA";
  attribute X_INTERFACE_INFO of s_axi_wvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WVALID";
  attribute X_INTERFACE_INFO of s_axi_wready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI WREADY";
  attribute X_INTERFACE_INFO of s_axi_bresp   : signal is "xilinx.com:interface:aximm:1.0 S_AXI BRESP";
  attribute X_INTERFACE_INFO of s_axi_bvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BVALID";
  attribute X_INTERFACE_INFO of s_axi_bready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI BREADY";
  attribute X_INTERFACE_INFO of s_axi_araddr  : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARADDR";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARVALID";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI ARREADY";
  attribute X_INTERFACE_INFO of s_axi_rdata   : signal is "xilinx.com:interface:aximm:1.0 S_AXI RDATA";
  attribute X_INTERFACE_INFO of s_axi_rvalid  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RVALID";
  attribute X_INTERFACE_INFO of s_axi_rready  : signal is "xilinx.com:interface:aximm:1.0 S_AXI RREADY";

  -- FSM for Fifo next element
  type FSM_FIFO_DELAY is (INIT, RESET, READY, WAIT_NOT_EMPTY, WAIT_DELAY, IDLE);
  signal i_fsm_fifo_delay : FSM_FIFO_DELAY;

  signal i_fsm_counter : INTEGER range 0 to 10;   -- internal fsm counter
  signal i_fifo_delay  : INTEGER range 0 to 2040; -- signal delay. 

  signal i_soft_reset : STD_LOGIC;

  -- signals for fifo:
  -- output from the fifo
  signal fifo_empty, fifo_full                  : STD_LOGIC;
  signal rd_rst_busy                            : STD_LOGIC;
  signal wr_rst_busy                            : STD_LOGIC;
  signal fifo_rd_data_count, fifo_wr_data_count : STD_LOGIC_VECTOR(8 downto 0);

  signal fifo_dbiterr, fifo_overflow, fifo_prog_empty, fifo_prog_full,
  fifo_sbiterr, fifo_underflow, fifo_wr_ack, fifo_injectdbiterr, fifo_injectsbiterr : STD_LOGIC;

  -- input to the fifo
  signal wr_en, rd_en : STD_LOGIC;
  signal rst          : STD_LOGIC;

  -- AXI signals
  signal i_axi_aw_ready    : STD_LOGIC;
  signal i_axi_wresp_stall : STD_LOGIC;
  signal i_s_axi_bvalid    : STD_LOGIC;
  signal i_s_axi_awready   : STD_LOGIC;
  signal i_s_axi_wready    : STD_LOGIC;

  -- axi data and address pre buffers
  signal i_s_axi_add_pre_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_pre_buff : STD_LOGIC_VECTOR(63 downto 0);

  -- axi data and address synced buffers
  signal i_s_axi_add_buff  : STD_LOGIC_VECTOR(7 downto 0);
  signal i_s_axi_data_buff : STD_LOGIC_VECTOR(63 downto 0);

  signal i_axi_valid_wraddr : STD_LOGIC;
  signal i_axi_valid_wdata  : STD_LOGIC;

  --axi read signals
  signal axi_arready : STD_LOGIC; -- axi signals
  signal axi_rvalid  : STD_LOGIC;
begin

  fsm : process (adc_clk, s_axi_aresetn, i_soft_reset)
  begin
    if (s_axi_aresetn = '0' or i_soft_reset = '1') then
      i_fsm_fifo_delay   <= INIT;
      rst                <= '0';
      wr_en              <= '0';
      rd_en              <= '0';
      s_axis_in_a_tready <= '0';
    elsif (rising_edge(adc_clk)) then

      case i_fsm_fifo_delay is

        when INIT =>
          if (sync = '1') then
            i_fsm_fifo_delay <= RESET;
            i_fsm_counter    <= 0;
            rst              <= '1';
          end if;
        when RESET => -- wait 3 clock cycles to reset the fifo fully
          i_fsm_counter <= i_fsm_counter + 1;

          if (i_fsm_counter >= 3 - 1) then
            i_fsm_counter    <= 0;
            i_fsm_fifo_delay <= READY;
            rst              <= '0';
          end if;

        when READY =>
          if wr_rst_busy = '0' and rd_rst_busy = '0' then -- we wait for the fifo to exit reset states on read and write channel.
            i_fsm_fifo_delay   <= WAIT_NOT_EMPTY;
            wr_en              <= '1'; -- enable fifo
            s_axis_in_a_tready <= '1'; -- axi stream ready signal
          end if;
        when WAIT_NOT_EMPTY =>
          if (fifo_empty = '0') then
            i_fsm_fifo_delay <= WAIT_DELAY;
            i_fsm_counter    <= 0; -- reset counter for next part
          end if;
        when WAIT_DELAY =>
          i_fsm_counter <= i_fsm_counter + 1;
          if (i_fsm_counter >= i_fifo_delay) then
            i_fsm_fifo_delay <= IDLE;
            rd_en            <= '1';
            i_fsm_counter    <= 0;
          end if;
        when IDLE =>
      end case;
    end if;
  end process fsm;

  xpm_fifo_sync_inst : xpm_fifo_sync
  generic map(
    DOUT_RESET_VALUE    => "0",        -- String
    ECC_MODE            => "no_ecc",   -- String
    FIFO_MEMORY_TYPE    => "auto",     -- String
    FIFO_READ_LATENCY   => 1,          -- DECIMAL
    FIFO_WRITE_DEPTH    => fifo_depth, -- DECIMAL
    FULL_RESET_VALUE    => 0,          -- DECIMAL
    PROG_EMPTY_THRESH   => 10,         -- DECIMAL
    PROG_FULL_THRESH    => 10,         -- DECIMAL
    RD_DATA_COUNT_WIDTH => 9,          -- DECIMAL
    READ_DATA_WIDTH     => (samples_per_adc*16),        -- DECIMAL
    READ_MODE           => "std",      -- String
    SIM_ASSERT_CHK      => 0,          -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
    USE_ADV_FEATURES    => "1707",     -- String
    WAKEUP_TIME         => 0,          -- DECIMAL
    WRITE_DATA_WIDTH    => (samples_per_adc*16),        -- DECIMAL
    WR_DATA_COUNT_WIDTH => 9           -- DECIMAL
  )
  port map(
    --almost_empty => almost_empty, -- 1-bit output: Almost Empty : When asserted, this signal indicates that
    -- only one more read can be performed before the FIFO goes to empty.

    --almost_full => almost_full, -- 1-bit output: Almost Full: When asserted, this signal indicates that
    -- only one more write can be performed before the FIFO is full.

    data_valid => s_axis_out_a_tvalid, -- 1-bit output: Read Data Valid: When asserted, this signal indicates
    -- that valid data is available on the output bus (dout).

    dbiterr => fifo_dbiterr, -- 1-bit output: Double Bit Error: Indicates that the ECC decoder
    -- detected a double-bit error and data in the FIFO core is corrupted.

    dout => s_axis_out_a_tdata, -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven
    -- when reading the FIFO.

    empty => fifo_empty, -- 1-bit output: Empty Flag: When asserted, this signal indicates that
    -- the FIFO is empty. Read requests are ignored when the FIFO is empty,
    -- initiating a read while empty is not destructive to the FIFO.

    full => fifo_full, -- 1-bit output: Full Flag: When asserted, this signal indicates that the
    -- FIFO is full. Write requests are ignored when the FIFO is full,
    -- initiating a write when the FIFO is full is not destructive to the
    -- contents of the FIFO.

    overflow => fifo_overflow, -- 1-bit output: Overflow: This signal indicates that a write request
    -- (wren) during the prior clock cycle was rejected, because the FIFO is
    -- full. Overflowing the FIFO is not destructive to the contents of the
    -- FIFO.

    prog_empty => fifo_prog_empty, -- 1-bit output: Programmable Empty: This signal is asserted when the
    -- number of words in the FIFO is less than or equal to the programmable
    -- empty threshold value. It is de-asserted when the number of words in
    -- the FIFO exceeds the programmable empty threshold value.

    prog_full => fifo_prog_full, -- 1-bit output: Programmable Full: This signal is asserted when the
    -- number of words in the FIFO is greater than or equal to the
    -- programmable full threshold value. It is de-asserted when the number
    -- of words in the FIFO is less than the programmable full threshold
    -- value.

    rd_data_count => fifo_rd_data_count, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates
    -- the number of words read from the FIFO.

    rd_rst_busy => rd_rst_busy, -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO
    -- read domain is currently in a reset state.

    sbiterr => fifo_sbiterr, -- 1-bit output: Single Bit Error: Indicates that the ECC decoder
    -- detected and fixed a single-bit error.

    underflow => fifo_underflow, -- 1-bit output: Underflow: Indicates that the read request (rd_en)
    -- during the previous clock cycle was rejected because the FIFO is
    -- empty. Under flowing the FIFO is not destructive to the FIFO.

    wr_ack => fifo_wr_ack, -- 1-bit output: Write Acknowledge: This signal indicates that a write
    -- request (wr_en) during the prior clock cycle is succeeded.

    wr_data_count => fifo_wr_data_count, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates
    -- the number of words written into the FIFO.

    wr_rst_busy => wr_rst_busy, -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO
    -- write domain is currently in a reset state.

    din => s_axis_in_a_tdata, -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when
    -- writing the FIFO.

    injectdbiterr => fifo_injectdbiterr, -- 1-bit input: Double Bit Error Injection: Injects a double bit error if
    -- the ECC feature is used on block RAMs or UltraRAM macros.

    injectsbiterr => fifo_injectsbiterr, -- 1-bit input: Single Bit Error Injection: Injects a single bit error if
    -- the ECC feature is used on block RAMs or UltraRAM macros.

    rd_en => rd_en, -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this
    -- signal causes data (on dout) to be read from the FIFO. Must be held
    -- active-low when rd_rst_busy is active high.

    rst => rst, -- 1-bit input: Reset: Must be synchronous to wr_clk. The clock(s) can be
    -- unstable at the time of applying reset, but reset must be released
    -- only after the clock(s) is/are stable.

    sleep => '0', -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo
    -- block is in power saving mode.

    wr_clk => adc_clk, -- 1-bit input: Write clock: Used for write operation. wr_clk must be a
    -- free running clock.

    wr_en => wr_en -- 1-bit input: Write Enable: If the FIFO is not full, asserting this
    -- signal causes data (on din) to be written to the FIFO Must be held
    -- active-low when rst or wr_rst_busy or rd_rst_busy is active high

  );

  -- !  AXI WRITE PROCESSES

  i_axi_wresp_stall  <= i_s_axi_bvalid and not s_axi_bready;  -- is our response channel stalled?
  i_axi_valid_wraddr <= s_axi_awvalid or not i_s_axi_awready; -- signals that we have a valid write address (awvalid or a buffered address (not awready))
  i_axi_valid_wdata  <= s_axi_wvalid or not i_s_axi_wready;   -- signals that we have a valid write data (same as above)

  s_axi_bvalid  <= i_s_axi_bvalid;
  s_axi_awready <= i_s_axi_awready;
  s_axi_wready  <= i_s_axi_wready;

  -- Axi address write ready signal assessing
  axi_aw_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_awready <= '1'; -- ready to get an address

      elsif (i_axi_wresp_stall = '1') then       -- if the response buffer is stalled, we can get an address only if nothing in buffer
        i_s_axi_awready <= not i_axi_valid_wraddr; -- if valid_wraddr is set, we have something in buffer or on input  and cannot get new address

      elsif (i_axi_valid_wdata = '1') then -- if a valid data is present, we don't care, we want the associated address
        i_s_axi_awready <= '1';

      else
        i_s_axi_awready <= i_s_axi_awready and not s_axi_awvalid; -- stay ready until we have a valid address incomming

      end if;
    end if;
  end process;

  -- Axi write ready signal assessing
  axi_w_ready : process (s_axi_aclk) -- we don't accept write bursts, as we juste have config as axilite
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_wready <= '1'; -- ready to get a data

      elsif (i_axi_wresp_stall = '1') then     -- if the response buffer is stalled, we can get a data only if nothing in buffer
        i_s_axi_wready <= not i_axi_valid_wdata; -- if valid_wdata is set, we have something in buffer or on input  and cannot get new data

      elsif (i_axi_valid_wraddr = '1') then -- if a valid address is present, we don't care, we want the associated data
        i_s_axi_wready <= '1';

      else
        i_s_axi_wready <= i_s_axi_wready and not s_axi_wvalid; -- stay ready until we have a valid data incomming

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write address
  axi_aw_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then -- pre buffer address when ready and valid
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_add_pre_buff <= s_axi_awaddr;

      end if;

      if (i_s_axi_awready = '0') then -- when not ready, we have something in buffer (address was here before data)
        i_s_axi_add_buff <= i_s_axi_add_pre_buff;

      else
        i_s_axi_add_buff <= s_axi_awaddr;

      end if;
    end if;
  end process;

  -- pre-buffering and buffering write data
  axi_w_buffer : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (i_s_axi_wready = '1' and s_axi_wvalid = '1') then
        i_s_axi_data_pre_buff <= s_axi_wdata;

      end if;

      if (i_s_axi_wready = '0') then -- when not ready, we have something in buffer (data was here before address)
        i_s_axi_data_buff <= i_s_axi_data_pre_buff;

      else
        i_s_axi_data_buff <= s_axi_wdata;

      end if;
    end if;
  end process;

  -- writing the data
  axi_w_write_data_to_regs : process (s_axi_aclk, s_axi_aresetn)
  begin
    if (s_axi_aresetn = '0') then
      i_fifo_delay <= 0;
      i_soft_reset <= '0';
    else
      if (rising_edge(s_axi_aclk)) then

        if (i_axi_wresp_stall = '0' and i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
          case i_s_axi_add_buff is

            when "00000000" => -- 0x00
              i_fifo_delay <= to_integer(unsigned(i_s_axi_data_buff(10 downto 0)));

            when "00001000" => -- 0x08
              i_soft_reset <= i_s_axi_data_buff(0);

            when others =>
          end case;
        end if;
      end if;
    end if;
  end process;

  -- response process

  s_axi_bresp <= "00"; -- always valid response for this module

  axi_bresp : process (s_axi_aclk)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        i_s_axi_bvalid <= '0';

      elsif (i_axi_valid_wraddr = '1' and i_axi_valid_wdata = '1') then
        i_s_axi_bvalid <= '1';

      else
        i_s_axi_bvalid <= '0';

      end if;
    end if;
  end process;

  axi_read : process (s_axi_aclk, s_axi_aresetn) -- Slow speed process (100MHz)
  begin
    if (rising_edge(s_axi_aclk)) then
      if (s_axi_aresetn = '0') then -- RESET

        axi_arready <= '0';
        axi_rvalid  <= '0';

      else
        if (s_axi_arvalid = '1' and axi_arready = '1') then -- AXI READ
          axi_arready <= '1';
          axi_rvalid  <= '1';

          s_axi_rdata <= (others => '0');

          if shift_right(unsigned(s_axi_araddr), 3) = 0 then -- (0x0000) read delay
            s_axi_rdata(9 downto 0) <= STD_LOGIC_VECTOR(to_unsigned(i_fifo_delay, 10));

          elsif shift_right(unsigned(s_axi_araddr), 3) = 1 then -- (0x0008) read errors if we have some
            s_axi_rdata(1) <= fifo_overflow;                      -- indicates an overflow error
            s_axi_rdata(2) <= fifo_underflow;
            s_axi_rdata(3) <= fifo_sbiterr;
            s_axi_rdata(4) <= fifo_dbiterr;
            s_axi_rdata(5) <= rd_rst_busy;
            s_axi_rdata(6) <= wr_rst_busy;

          else -- Other addresses return DEADBEEFDEADBEEF to AXI
            s_axi_rdata <= x"DEADBEEFDEADBEEF";

          end if;
        else -- Reset AXI signals after the read
          axi_rvalid  <= '0';
          axi_arready <= '1';

        end if;

      end if;
    end if;
  end process;

  s_axi_rvalid  <= axi_rvalid;
  s_axi_arready <= axi_arready;

end Behavioral;