----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.11.2021 15:20:56
-- Design Name: 
-- Module Name: Led_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity Led_control is
  port (
    LEDs : out STD_LOGIC_VECTOR (3 downto 0);

    adc_clk : in STD_LOGIC;

    ext_PLL_muxout : in STD_LOGIC_VECTOR(0 downto 0);

    adc_a_or : in STD_LOGIC;
    adc_b_or : in STD_LOGIC;

    adc_pll_locked : in STD_LOGIC);
end Led_control;

architecture Behavioral of Led_control is

  signal i_pll_cnt  : INTEGER range 0 to 250000000;
  signal i_leds_clk : STD_LOGIC;

begin

  adc_led_blink : process (adc_clk)
  begin
    if (rising_edge(adc_clk)) then

      i_pll_cnt <= i_pll_cnt + 1;

      if (i_pll_cnt >= 250000000) then
        i_leds_clk <= not i_leds_clk;
        i_pll_cnt  <= 0;
      end if;

    end if;
  end process adc_led_blink;

  LEDs(0) <= adc_a_or or adc_b_or;
  LEDs(1) <= adc_pll_locked;
  LEDs(2) <= i_leds_clk;
  LEDs(3) <= ext_PLL_muxout(0);

end Behavioral;