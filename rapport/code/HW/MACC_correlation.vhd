----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Philipp Schuler
-- 
-- Create Date: 16.11.2021 21:20:13
-- Design Name: 
-- Module Name: MACC_correlation - Behavioral
-- Project Name: 
-- Target Devices: htg-zrf8
-- Tool Versions: Vivado 2020.2
-- Description: This modules does one multiply accumulate for the correlation module.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 1.00 - File tested and cleaned up
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity MACC_correlation is
  generic (
    input_size  : INTEGER := 12;
    output_size : INTEGER := 41
  );
  port (
    clk   : in STD_LOGIC;
    a     : in STD_LOGIC_VECTOR(input_size - 1 downto 0);
    b     : in STD_LOGIC_VECTOR(input_size - 1 downto 0);
    sload : in STD_LOGIC;

    reg_out : out STD_LOGIC_VECTOR(output_size - 1 downto 0)
  );
end MACC_correlation;

architecture rtl of MACC_correlation is

  signal a_reg, b_reg          : signed (input_size - 1 downto 0) := to_signed(0, input_size);
  signal sload_reg             : STD_LOGIC;
  signal mult_reg              : signed (2 * input_size - 1 downto 0) := to_signed(0, 2*input_size);
  signal adder_out, old_result : signed (output_size - 1 downto 0)    := to_signed(0, output_size);

  -- We force the use of DSPs
  attribute USE_DSP              : STRING;
  attribute USE_DSP of adder_out : signal is "YES";

begin

  process (adder_out, sload_reg)
  begin
    if sload_reg = '1' then
      old_result <= (others => '0');
    else
      -- 'sload' is now active (=low) and opens the accumulation loop.
      -- The accumulator takes the next multiplier output in
      -- the same cycle.
      old_result <= adder_out;
    end if;
  end process;

  process (clk)
  begin
    if rising_edge(clk) then
      a_reg     <= signed(a);
      b_reg     <= signed(b);
      mult_reg  <= a_reg * b_reg;
      sload_reg <= sload;
      -- Store accumulation result into a register
      adder_out <= old_result + mult_reg;
      if (sload_reg = '1') then
        reg_out <= STD_LOGIC_VECTOR(adder_out);
      end if;
    end if;
  end process;

end rtl;