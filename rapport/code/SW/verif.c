#include <string>
#include <iostream>
#include <chrono>
#include <fstream>
#include <sstream>
#include <math.h>

using namespace std;

string row;
int batch;
int number;
int chan1;
int64_t sigma_x[8];
int64_t sigma_x2[8];
int64_t sigma_y[8];
int64_t sigma_y2[8];
int64_t test[8];
int64_t corr[800];
int64_t calculated_corr[8];
int chan2;
char comma;

int sx[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int s2x[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int sy[8] = {0, 0, 0, 0, 0, 0, 0, 0};
int s2y[8] = {0, 0, 0, 0, 0, 0, 0, 0};


int main()
{
    fstream file;
    ofstream file_out;
    fstream filesigma;
    fstream filecorr;
    string read_fifo = "fifo_6592.txt";
    string sigmaName = "sigma.txt";
    string corrName = "corr.txt";
    file.open(read_fifo);
    //file_out.open(save_fifo);
    filesigma.open(sigmaName);
    filecorr.open(corrName);
    int i = 0;
    while (true)
    {
        file >> batch >> comma >> number >> comma >> chan1 >> comma >> chan2;
        if (file.fail() || file.eof())
        {
            cout << "finished" << endl;
            break;
        }

        volatile short convert = chan1;
        volatile short shifted = convert >> 4;
        volatile short convert2 = chan2;
        volatile short shifted2 = convert2 >> 4;
        sx[i % 8] += shifted;
        sy[i % 8] += shifted2;
        calculated_corr[i%8] += shifted*shifted2;

        //cout << chan1 << endl << convert << endl << shifted << endl << s2y << endl;
        //file_out << batch << "," << number << "," << shifted  << "," << shifted2 << endl;
        //file.ignore (128, '\n');
        //break;
        i++;
    }

    while (true)
    {
        int batch_sigma;
        string other;
        filesigma >> batch_sigma >> comma >> sigma_x[0] >> comma >> sigma_x[1] >> comma >> sigma_x[2] >> comma >> sigma_x[3] >> comma >> sigma_x[4] >> comma >> sigma_x[5] >> comma >> sigma_x[6] >> comma >> sigma_x[7] >> comma >> sigma_x2[0] >> comma >> sigma_x2[1] >> comma >> sigma_x2[2] >> comma >> sigma_x2[3] >> comma >> sigma_x2[4] >> comma >> sigma_x2[5] >> comma >> sigma_x2[6] >> comma >> sigma_x2[7];
        //cout << batch_sigma << " " << batch*10 << endl;
        //cout << batch_sigma << endl;
        //break;
        if (batch_sigma == batch * 10)
        {
            cout << "Found Sigma X" << endl;
            break;
        }
        if (filesigma.fail() || filesigma.eof())
        {
            cout << "finished sigma" << endl;
            break;
        }
    }
    while (true)
    {
        int batch_sigma;
        string other;
        filesigma >> batch_sigma >> comma >> sigma_y[0] >> comma >> sigma_y[1] >> comma >> sigma_y[2] >> comma >> sigma_y[3] >> comma >> sigma_y[4] >> comma >> sigma_y[5] >> comma >> sigma_y[6] >> comma >> sigma_y[7] >> comma >> sigma_y2[0] >> comma >> sigma_y2[1] >> comma >> sigma_y2[2] >> comma >> sigma_y2[3] >> comma >> sigma_y2[4] >> comma >> sigma_y2[5] >> comma >> sigma_y2[6] >> comma >> sigma_y2[7];
        //cout << batch_sigma << " " << batch*10 << endl;
        //cout << batch_sigma << endl;
        //break;
        if (batch_sigma == (batch * 10) + 1)
        {
            cout << "Found sigma Y" << endl;
            break;
        }
        if (filesigma.fail() || filesigma.eof())
        {
            cout << "finished sigma" << endl;
            break;
        }
    }
    while (true)
    {
        int batch_corr;
        int channelA, channelB;
        filecorr >> batch_corr >> comma >> channelA >> comma >> channelB;
        for (int k = 0; k < 800; k++)
        {
            filecorr >> comma >> corr[k];
            //cout << corr[k] << ":";
        }
        //cout << batch_sigma << " " << batch*10 << endl;
        //cout << batch_corr << endl;
        //break;
        if (batch_corr == (batch))
        {
            cout << "Found correlation" << endl;
            for (int m = 0; m < 800; m++)
            {
                if (corr[m] & (1ULL << 47))
                {
                    corr[m] = ~corr[m] & 0xFFFFFFFFFFFFULL;
                    corr[m] += 1;
                    corr[m] *= -1;
                }
            }
            break;
        }
        if (filecorr.fail() || filecorr.eof())
        {
            cout << "finished corr" << endl;
            break;
        }
    }
    file.close();
    file_out.close();
    for (int j = 0; j < 8; j++)
    {
            test[j] = sigma_x[j];
        if (sigma_x[j] & (1ULL << 47))
        {
            test[j] -= 2* (1ULL<<47);
            sigma_x[j] = ~sigma_x[j] & 0xFFFFFFFFFFFFULL;
            sigma_x[j] += 1;
            sigma_x[j] *= -1;
        }
        if (sigma_y[j] & (1ULL << 47))
        {
            sigma_y[j] = ~sigma_y[j] & 0xFFFFFFFFFFFFULL;
            sigma_y[j] += 1;
            sigma_y[j] *= -1;
        }
        cout << "sigma X" << j << " = " << sx[7 - j] << "\t:\t" << sigma_x[j] << " -> " << test[j] << (sx[7 - j] == sigma_x[j] ? "\tOK" : "\tNOK") << endl;
        cout << "sigma Y" << j << " = " << sy[7 - j] << "\t:\t" << sigma_y[j] << (sy[7 - j] == sigma_y[j] ? "\tOK" : "\tNOK") << endl;
        cout << "corr" << j << " = " << corr[7-j] << "\t:\t" << calculated_corr[j] << (corr[7 - j] == calculated_corr[j] ? "\tOK" : "\tNOK") << endl;
    }
}