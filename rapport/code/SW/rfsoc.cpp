//
//
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/fcntl.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <chrono>
#include <ctime>

#include "squared_accumulators_data_structure.h"

// for using strtol
#include <stdlib.h>

#include "rfsoc.hpp"
using namespace std;
class RFSOC
{
    // main infos

    // batch Fifo storage
    volatile unsigned short data_batch_a[131072 * 8];
    volatile unsigned short data_batch_b[131072 * 8];

    volatile unsigned int *batch = NULL;
    volatile unsigned int *sync = NULL;
    volatile unsigned int *general_infos_register = NULL;
    volatile unsigned int actual_batch = 0;
    volatile unsigned int previous_sent_batch = 0;
    unsigned int parallel_batches = 8;

    //Correlation Base addresses
    volatile unsigned long *correlation[15];

    // Corr
    const unsigned long corr_add[1] = {0xA00E0000};
    const unsigned int corr_channel_1[1] = {0};
    const unsigned int corr_channel_2[1] = {1};
    // const unsigned int corr_chan_1[15] = {1,1,1,1,1,2,2,2,2,3,3,3,4,4,5};
    // const unsigned int corr_chan_2[15] = {2,3,4,5,6,3,4,5,6,4,5,6,5,6,6};
    volatile unsigned int correlation_total_count = 0;
    volatile unsigned int correlation_width = 0;

    // FIFO
    volatile unsigned long *hi_fifo_a = NULL;
    volatile unsigned long *lo_fifo_a = NULL;
    volatile unsigned long *fifo_control = NULL;
    volatile unsigned long *hi_fifo_b = NULL;
    volatile unsigned long *lo_fifo_b = NULL;
    volatile unsigned long *batch_fifo = NULL;

    //SIGMAs
    volatile unsigned long *sigma_reg[MAX_CHANNELS];
    const unsigned long sigma_add[MAX_CHANNELS] = {0xa00d0000, 0xa0030000};
    volatile unsigned int sigma_total_count = 0;

    //Delayers
    volatile unsigned long *delayers[3];
    const unsigned long delayers_add[3] = {0xa0100000, 0xa0110000, 0xa0120000};

    //Signal injector
    volatile unsigned long *injector;

    // Register values holders
    volatile unsigned long sigma_vals[MAX_CHANNELS][8];
    volatile unsigned long sigma_vals_squared[MAX_CHANNELS][8];
    volatile unsigned long corr_vals[1][832];

    //  data_structures for zmqpp
    AccumulatorsSigma sigma_struct[MAX_CHANNELS];
    AccumulatorsCorr correlation_struct[3];
    GeneralInfos general_infos;
    FifoX2 fifo_struct;

public:
    int version = 0;

    RFSOC()
    {
        fd = open("/dev/mem", O_RDWR | O_SYNC);

        if (fd == -1)
        {
            perror("Failed to open /dev/mem ");
            exit(-1);
        }
        batch = set(0xA0080000);
        sync = set(0xA0020008);
        general_infos_register = set(SYSTEM_CTRL_BASE);

        *sync = 0; // need to go out of sync for correlation accums (bug to fix)

        batch_fifo = setlong(FIFO_BASE);
        lo_fifo_a = setlong(0xA00C0008);
        hi_fifo_a = setlong(0xA00C0010);
        lo_fifo_b = setlong(0xA00C0018);
        hi_fifo_b = setlong(0xA00C0020);
        fifo_control = setlong(0xA00C0028);
        injector = setlong(0xA00F0000);

        // Set the pointers to the sigma accumulators
        for (int i = 0; i < 2; i++)
        {
            sigma_reg[i] = setlong(sigma_add[i]);
            delayers[i] = setlong(delayers_add[i]);
            if (sigma_reg[i] != NULL)
                sigma_total_count += 1;
        }

        // Set the pointers to the correlation accumulators and counts the number of accumulators set
        for (int i = 0; i < 1; i++)
        {
            correlation[i] = setlong(corr_add[i]);
            if (correlation[i] != NULL)
                correlation_total_count += 1;
        }
        if (correlation_total_count != 0)
        {
            correlation_width = (int)(*(correlation[0]) & 0xFF) * 8;
        }

        time_t bitstream_implementation_time;
        bitstream_implementation_time = get_pl_timestamp(*general_infos_register);

        general_infos.frequency = get_frequency();
        general_infos.hardwareVersion = bitstream_implementation_time;
        general_infos.batch_size = *(sigma_reg[0])&0x1FFFFFF;
        general_infos.parallel_data_count = parallel_batches;

        cout << "Bistream date: " << bitstream_implementation_time << endl;

        cout << "Connected to PL" << endl;
    }

    time_t get_pl_timestamp(unsigned int bitstream_timestamp_value)
    {
        //volatile int year, month, day, hours, minutes, seconds;
        time_t rawtime;
        time_t timestamp;
        struct tm *timestruct;
        time(&rawtime);
        timestruct = localtime(&rawtime);

        timestruct->tm_mday = (bitstream_timestamp_value >> (32 - 5)) & 0x1F;
        timestruct->tm_mon = ((bitstream_timestamp_value >> (32 - 5 - 4)) & 0x0F) - 1;
        timestruct->tm_year = ((bitstream_timestamp_value >> (32 - 5 - 4 - 6)) & 0x3F) + 100;
        timestruct->tm_hour = (bitstream_timestamp_value >> (32 - 5 - 4 - 6 - 5)) & 0x1F;
        timestruct->tm_min = (bitstream_timestamp_value >> (32 - 5 - 4 - 6 - 5 - 6)) & 0x3F;
        timestruct->tm_sec = (bitstream_timestamp_value >> (32 - 5 - 4 - 6 - 5 - 6 - 6)) & 0x3F;

        cout << timestruct->tm_mday << "/" << timestruct->tm_mon << "/" << timestruct->tm_year << endl;
        timestamp = timegm(timestruct);
        cout << timestamp << endl;

        return timestamp;
    }

    void set_injection_values(uint64_t x0, uint64_t y0)
    {
        *(injector + 1) = x0;
        *(injector + 2) = 0;
        *(injector + 3) = y0;
        *(injector + 4) = 0;
    }

    void set_injection_status(bool active)
    {
        if (active)
        {
            *injector = 1;
        }
        else
        {
            *injector = 0;
        }
    }

    void sync_accumulators()
    {
        *sync = 1;
        usleep(1000); //need to wait minimum 3 clock cycles so we wait 1 milisecond
        *sync = 0;
    }

    // Set the batch size in all modules
    void set_batch_sizes(int batch_size)
    {
        for (int channel = 0; channel < sigma_total_count; channel++)
        {
            *(sigma_reg[channel]) = batch_size;
            if (*(sigma_reg[channel]) & 0x1FFFFFF != batch_size)
            {
                cout << "ERROR: batch size was not set properly on sigma accumulator " << channel << endl;
                exit(1);
            }
        }
        for (int channel = 0; channel < correlation_total_count; channel++)
        {
            *(correlation[channel]) = batch_size;
            if (*(correlation[channel]+1) != batch_size)
            {
                cout << "ERROR: batch size was not set properly on correlation accumulator " << channel << endl;
                exit(1);
            }
        }
    }

    // Read all the data from all register
    void update_data()
    {
        int count = 0;
        while (actual_batch == *batch)
            ;
        do
        {
            actual_batch = *batch;
            // Updating all the sigmas
            for (int channel = 0; channel < sigma_total_count; channel++)
            {
                sigma_struct[channel].batch = actual_batch;
                sigma_struct[channel].channel = channel;
                for (int i = 0; i < 8; i++)
                {
                    sigma_struct[channel].sum[i] = *(sigma_reg[channel] + i + 1);
                    sigma_struct[channel].sum2[i] = *(sigma_reg[channel] + SIGMA_MEAN2_OFFSET + i + 1);
                }
            }
            // Updating all the correlations
            for (int channel = 0; channel < correlation_total_count; channel++)
            {
                correlation_struct[channel].batch = actual_batch;
                correlation_struct[channel].real_corr_size = *(correlation[channel]) & 0xFF;
                correlation_struct[channel].channel1 = corr_channel_1[channel];
                correlation_struct[channel].channel2 = corr_channel_2[channel];
                for (int w = 0; w < correlation_width; w++)
                {
                    correlation_struct[channel].corr[w] = *(correlation[channel] + w + 2);
                }
            }
            count++;
        } while (*batch != actual_batch);

        if (count > 1)
        {
            cout << "Lost " << count - 1 << " batches!" << endl;
        }
        else if ((actual_batch - previous_sent_batch) > 1)
        {
            cout << "Missed " << (actual_batch - previous_sent_batch) - 1 << " batches!" << endl;
        }
        previous_sent_batch = actual_batch;
    }

    void set_delays(long channel_a, long channel_b, long channel_c /*, long channel_d, long channel_e, long channel_f*/)
    {
        if (delayers[0] != NULL)
        {
            *delayers[0] = channel_a;
            *(delayers[0] + 1) = 1; // soft reset the delayer
            usleep(100);
            *(delayers[0] + 1) = 0; // soft reset the delayer
            general_infos.delays[0] = *delayers[0];
        }

        if (delayers[1] != NULL)
        {
            *delayers[1] = channel_b;
            *(delayers[1] + 1) = 1; // soft reset the delayer
            usleep(100);
            *(delayers[1] + 1) = 0; // soft reset the delayer
            general_infos.delays[1] = *delayers[1];
        }

        if (delayers[2] != NULL)
        {
            *delayers[2] = channel_c;
            *(delayers[2] + 1) = 1; // soft reset the delayer
            usleep(100);
            *(delayers[2] + 1) = 0; // soft reset the delayer
            general_infos.delays[2] = *delayers[2];
        }

        /*if (delayers[3] != NULL)
        {
            *delayers[3] = channel_d;
            *(delayers[3] + 1) = 1; // soft reset the delayer
            general_infos.delays[3];
        }

        if (delayers[4] != NULL)
        {
            *delayers[4] = channel_e;
            *(delayers[4] + 1) = 1; // soft reset the delayer
            general_infos.delays[4];
        }

        if (delayers[5] != NULL)
        {
            *delayers[5] = channel_f;
            *(delayers[5] + 1) = 1; // soft reset the delayer
            general_infos.delays[5];
        }*/

        // need to resync the delayers and accumulators
        sync_accumulators(); // for now, sync is done with reset
    }

    unsigned int get_frequency()
    {
        // 0xa0160008 -> we get the freq in MHz of the PL part -> time parallel batches = freq * 8 = sampling freq
        volatile unsigned int pl_freq = *(general_infos_register + 2);
        unsigned int ADC_frequency = (unsigned int)((double)(pl_freq * parallel_batches));

        return ADC_frequency;
    }

    AccumulatorsSigma get_sigmas(int channel)
    {
        return sigma_struct[channel];
    }

    AccumulatorsCorr get_corrs(int index)
    {
        return correlation_struct[index];
    }

    GeneralInfos get_general_infos()
    {
        return general_infos;
    }

    FifoX2 get_fifos()
    {
        return fifo_struct;
    }

    int get_batch()
    {
        return actual_batch;
    }

    void fifo_capture()
    {
        cout << "Capturing fifo" << endl;
        *fifo_control = 1;
        cout << *fifo_control << endl;
    }

    void read_fifo()
    {

        // we get data on the second read only:
        //      cout << "\nFifo next 2x" << endl;
        *fifo_control = FIFO_CTL_NEXT;
        // THERE MUST BE A READ BETWEEN THE TWO WRITES OTHERWISE THE CARD DIES
        fifo_struct.batch = *batch_fifo;
        *fifo_control = FIFO_CTL_NEXT;

        for (int i = 0; i < FIFO_SAMPLE_TOTAL_COUNT; i++)
        {
            unsigned long low_bits_a = *lo_fifo_a;
            unsigned long high_bits_a = *hi_fifo_a;
            unsigned long low_bits_b = *lo_fifo_b;
            unsigned long high_bits_b = *hi_fifo_b;

            fifo_struct.data_batch_a[i * 8 + 0] = (low_bits_a >> 0 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 1] = (low_bits_a >> 16 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 2] = (low_bits_a >> 32 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 3] = (low_bits_a >> 48 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 4] = (high_bits_a >> 0 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 5] = (high_bits_a >> 16 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 6] = (high_bits_a >> 32 & 0xFFFF);
            fifo_struct.data_batch_a[i * 8 + 7] = (high_bits_a >> 48 & 0xFFFF);

            fifo_struct.data_batch_b[i * 8 + 0] = (low_bits_b >> 0 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 1] = (low_bits_b >> 16 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 2] = (low_bits_b >> 32 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 3] = (low_bits_b >> 48 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 4] = (high_bits_b >> 0 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 5] = (high_bits_b >> 16 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 6] = (high_bits_b >> 32 & 0xFFFF);
            fifo_struct.data_batch_b[i * 8 + 7] = (high_bits_b >> 48 & 0xFFFF);

            *fifo_control = FIFO_CTL_NEXT;
        }
    }

private:
    int fd = 0;
    volatile unsigned int *set(unsigned long int_address)
    {
        volatile unsigned int *start = NULL;
        off_t page_start = int_address & ~(getpagesize() - 1);
        start = (unsigned int *)mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, page_start);
        if (start == MAP_FAILED)
        {
            ostringstream str;
            str << "ERROR with mmap at address " << int_address;
            perror(str.str().c_str());
            return NULL;
        }
        return start + (int_address & (getpagesize() - 1)) / sizeof(unsigned int);
    }

    volatile unsigned long *setlong(unsigned long int_address)
    {
        volatile unsigned long *startlong = NULL;
        unsigned long page_start = int_address & ~(off_t)(getpagesize() - 1);
        unsigned mapped_size = getpagesize() * 2;

        startlong = (unsigned long *)mmap(NULL, mapped_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, page_start);

        if (startlong == MAP_FAILED)
        {
            ostringstream str;
            str << "ERROR with mmap at address " << int_address;
            perror(str.str().c_str());
            return NULL;
        }

        return startlong + (int_address & (getpagesize() - 1)) / sizeof(unsigned long);
    }
};