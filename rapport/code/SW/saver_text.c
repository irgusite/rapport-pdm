#include <zmqpp/zmqpp.hpp>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <sstream>
#include <math.h>
#include "../accums_readout/squared_accumulators_data_structure.h"

using namespace std;

// const string client_endpoint = "tcp://10.194.168.72:5555";
const string client_endpoint = "tcp://192.168.10.56:5555";
//const string client_endpoint = "tcp://84.226.193.172:5555";

const int n_acc = 8;

int main(int argc, char *argv[])
{
	AccumulatorsX8 accums;
	FifoX2 fifos;
	AccumulatorsSigma sigma;
	AccumulatorsCorr corr;


	// initialize the 0MQ context
	zmqpp::context context;

	// generate a pull socket
	zmqpp::socket_type client_type = zmqpp::socket_type::pull;

	cout << "Starting the monitor" << endl;
	zmqpp::socket socket(context, client_type);

	// bind to the socket
	socket.connect(client_endpoint);

	while (1)
	{

		// receive the message
		zmqpp::message message;
		// decompose the message

		bool ok = socket.receive(message, true);
		if (ok)
		{
			for (unsigned int payload_id = 0; payload_id < message.parts(); payload_id++)
			{
				// cout << "Message received" << endl;
				unsigned int type = *(unsigned int *)((message.raw_data(payload_id)));
				if (type == FIFOX2TAG)
				{
					cout << "FIFO received " << hex << type << dec << endl;
					memcpy(reinterpret_cast<char *>(&fifos), message.raw_data(payload_id), sizeof(FifoX2));
					// t2->Fill();
					n_fifo++;
					cout << dec << n_fifo << " " << fifos.batch << " " << hex << fifos.data_batch_a[0] << " " << fifos.data_batch_b[0] << endl;
					ofstream file;
					std::string folder = "rfsoc_files/fifo_";
					std::string save_fifo = folder + std::to_string(fifos.batch) + ".txt";
					file.open(save_fifo);
					for (int k = 0; k < 32768 * 8; k++)
					{
						file << fifos.batch << "," << k << "," << fifos.data_batch_a[k] << "," << fifos.data_batch_b[k] << endl;
					}
					file.close();
					//last_fifo_batch = fifos.batch;

					if (n_fifo % 10 == 0)
					{
						// t2->AutoSave("SaveSelf");
						cout << dec << n_fifo << " " << fifos.batch << " " << hex << fifos.data_batch_a[0] << " " << fifos.data_batch_b[0] << endl;
					}
				}
				else if (type == ACCUMXSIGMA)
				{
					assert(sizeof(AccumulatorsSigma) == message.size(payload_id));
					assert(type == ACCUMXSIGMA);

					memcpy(reinterpret_cast<char *>(&sigma), message.raw_data(payload_id), sizeof(AccumulatorsSigma));

					if (true)
					{

						ofstream file;
						std::string folder = "rfsoc_files/sigma";
						std::string save_fifo = folder + ".txt";
						file.open(save_fifo, ios_base::app);
						file << sigma.batch;
						file << sigma.channel;
						for (int k = 0; k < n_acc; k++)
						{
							file << "," << sigma.sum[k];
						}
						for (int k = 0; k < n_acc; k++)
						{
							file << "," << sigma.sum2[k];
						}
						file << endl;
						file.close();
					}

				}
				else if (type == ACCUMXCORR)
				{
					assert(sizeof(AccumulatorsCorr) == message.size(payload_id));
					assert(type == ACCUMXCORR);

					memcpy(reinterpret_cast<char *>(&corr), message.raw_data(payload_id), sizeof(AccumulatorsCorr));

					if (true)
					{

						ofstream file;
						std::string folder = "rfsoc_files/corr";
						std::string save_fifo = folder + ".txt";
						file.open(save_fifo, ios_base::app);
						file << corr.batch << ",";
						file << corr.channel1 << ",";
						file << corr.channel2;
						for (int k = 0; k < 800; k++)
						{
							file << "," << corr.corr[k];
						}
						file << endl;
						file.close();
					}

				}
			}
		}
		else
		{
			if (!already)
			{
				cout << "Waiting..." << endl;
				already = true;
			}
		}
	}
	// t->Write();
	// t2->Write();
	// delete f;
}
