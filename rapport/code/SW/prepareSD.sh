#!/bin/bash

printf "\n\n**************************************************************\n"
printf "RFSoC SD card preparation script\n"
printf "BE AWARE: this can destroy your computer if not used correctly\n"
printf "Some manipulations rm -rf the whole root folder of a partition\n"
printf '**************************************************************\n\'n

echo "Please input the SD card device. (Example: /dev/sdb)"
read -e -p "SD card device [/dev/sdX]:" sdcard
umount ${sdcard}1
umount ${sdcard}2
echo $sdcard


echo "Mouting $sdcard to folders /mnt/rfsoc_sd_root and /mnt rfsoc_sd_boot"
mkdir -p /mnt/rfsoc_sd_root
mkdir -p /mnt/rfsoc_sd_boot
mount ${sdcard}1 /mnt/rfsoc_sd_boot
mount ${sdcard}2 /mnt/rfsoc_sd_root

echo "Clearing boot folder?"
read -p "[y/n]:" bootkeep
if [ $bootkeep = "y" ]
then
	rm -rf /mnt/rfsoc_sd_boot/*
	echo "Boot partition cleared"
else
	echo "Boot partition kept as is"
fi

echo "Clearing rootfs folder?"
read -p "[y/n]:" rootfskeeping
if [ $rootfskeeping = "y" ]
then
	rm -rf /mnt/rfsoc_sd_root/*
	echo "RootFS cleared"
else
	echo "RootFS kept intact"
fi

echo "Preparing SD card"
echo "Please enter folder containing BOOT.BIN, image.ub and rootfs.tar.gz"
read -e -p "Folder: " sdfilesfolder
echo $sdfilesfolder

cd $sdfilesfolder

if [ $bootkeep = "y" ]
then
	cp BOOT.BIN /mnt/rfsoc_sd_boot
	cp image.ub /mnt/rfsoc_sd_boot
fi

if [ $rootfskeeping = "y" ]
then
	tar xvf rootfs.tar.gz -C /mnt/rfsoc_sd_root
fi

ls /mnt/rfsoc_sd_boot
ls /mnt/rfsoc_sd_root

umount ${sdcard}1
umount ${sdcard}2
