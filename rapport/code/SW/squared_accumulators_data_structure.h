#ifndef SADSTRUCT_H
#define SADSTRUCT_H

#define NUM_BUSSES 8
#define CORR_SIZE 800
#define FIFO_LENGTH 262144
#define NUM_CHANNELS 6

#define ACCUMX8TAG 0x5555
#define ACCUMXSIGMA 0xFFFF
#define ACCUMXCORR 0xEEEE
#define GENERALINFOS 0x1111
#define FIFOX2TAG 0xAAAA

struct AccumulatorsX8
{
    AccumulatorsX8() : header(ACCUMX8TAG) {}
    unsigned int header;
    long long sumx[NUM_BUSSES];
    long long sum2x[NUM_BUSSES];
    long long sumy[NUM_BUSSES];
    long long sum2y[NUM_BUSSES];
    long long corr[CORR_SIZE];
    int real_corr_size;
    int batch;
};

struct AccumulatorsSigma
{
    AccumulatorsSigma() : header(ACCUMXSIGMA) {}
    unsigned int header;
    unsigned int channel;
    long long sum[NUM_BUSSES];
    long long sum2[NUM_BUSSES];
    unsigned int batch;
};

struct AccumulatorsCorr
{
    AccumulatorsCorr() : header(ACCUMXCORR) {}
    unsigned int header;
    unsigned int channel1;
    unsigned int channel2;
    long long corr[CORR_SIZE];
    int real_corr_size;
    unsigned int batch;
};

struct GeneralInfos
{
    GeneralInfos() : header(GENERALINFOS) {}
    unsigned int header;
    unsigned int frequency;
    unsigned int delays[NUM_CHANNELS];
    unsigned int batch_size; // number of samples per batch
    unsigned int parallel_data_count; // number of samples treated in parallel
    unsigned int nb_channels;
    time_t hardwareVersion;
};

struct FifoX2
{
    FifoX2() : header(FIFOX2TAG) {}
    unsigned int header;

    unsigned int batch;
    unsigned short data_batch_a[FIFO_LENGTH];
    unsigned short data_batch_b[FIFO_LENGTH];
};

#endif
