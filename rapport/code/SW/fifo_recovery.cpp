// BUILD ME: g++ -o fifo fifo_recovery.cpp -std=c++11 -lzmqpp -lzmq
// export LD_LIBRARY_PATH=/usr/local/lib64/:/usr/local/lib/

#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/fcntl.h>
#include <stdlib.h>

#include <zmqpp/zmqpp.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <chrono>
#include <string>

#include "squared_accumulators_data_structure.h"

// for using strtol
#include <stdlib.h>

#include "rfsoc.cpp"

using namespace std;

void init()
{
}

void finish()
{
  //stop_the_tile();
  // munmap((void *)(start), getpagesize());
  // close(fd);
}

zmqpp::context context;
zmqpp::socket socket(context, zmqpp::socket_type::push);

#define TOTAL_PAYLOADS 10

char *data_to_send[TOTAL_PAYLOADS];
int data_sizes[TOTAL_PAYLOADS];
int num_payloads = 0;
void sendZmqMultiMessage(void *data, int size)
{
  data_to_send[num_payloads] = new char[size];
  memcpy(data_to_send[num_payloads], data, size);
  data_sizes[num_payloads] = size;
  num_payloads++;
  if (num_payloads == TOTAL_PAYLOADS)
  {
    zmqpp::message message;
    for (int i = 0; i < TOTAL_PAYLOADS; i++)
    {
      message.add_raw(data_to_send[i], data_sizes[i]);
      delete[] data_to_send[i];
    }
    socket.send(message);
    num_payloads = 0;
  }
}

int main(int argc, char **argv)
{
  AccumulatorsSigma sigma_struct;
  AccumulatorsCorr correlation_struct;
  GeneralInfos general_infos;
  FifoX2 fifo_struct;

  // Reset the modules
  int delay0 = 0;
  int delay1 = 0;
  int delay2 = 0;
  int delay3 = 0;
  int delay4 = 0;
  int delay5 = 0;

  bool capture_signal = false;

  int batch_size = 1 << 17;

  int nb_channels = 2;

  // Getting the command line arguments
  for (int i = 1; i < argc; i++)
  {
    if (i + 1 != argc)
    {
      if (strcmp(argv[i], "-delayA") == 0)
      {
        delay0 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-delayB") == 0)
      {
        delay1 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-delayC") == 0)
      {
        delay2 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-delayD") == 0)
      {
        delay3 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-delayE") == 0)
      {
        delay4 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-delayF") == 0)
      {
        delay5 = stoi(argv[i + 1], NULL);
        i++; // go to next flag
      }
      else if (strcmp(argv[i], "-capture-signal") == 0) // This activates the signal capture every 1000 batches
      {
        capture_signal = true;
        cout << "Signal Capture enabled" << endl;
      }
      else if (strcmp(argv[i], "-nb_channels") == 0)
      {
        nb_channels = stoi(argv[i + 1], NULL);
        if (nb_channels < 0 || nb_channels > 6)
        {
          cout << "ERROR: number of channels must be comprised between 2 and 6" << endl;
          exit(1);
        }
        i++;
      }
      else if (strcmp(argv[i], "-batch-size") == 0)
      {
        int size = stoi(argv[i + 1], NULL);
        if (size > 24 or size < 10)
        {
          cout << "ERROR: Wrong batch size, must be comprised between 10 and 24 (corresponds to 1<<10 and 1<<24)" << endl;
          exit(1);
        }
        batch_size = 1 << size;
        i++;
      }
      else
      {
        // cout << "Data Aquisition system for RFSoC\n
                
        //           \n\t - delayX n : adds a delay of n clock cycles to channel X(A - F)
        //           \n\t - batch - size n : Overrides the default batch size of 1 << 18 to 1 << n
        //           \n\t - capture - signal : Captures the input signal from Channel A and B approximately every 1000 batches
        //           \n\t - nb_channels n : Overrides the number of channels sent in the general_info packet " << endl;
        //                                   exit(0);
      }
    }
  }

  socket.bind("tcp://*:5555");

  // if (argc == 4)
  // {
  //   cout << "We have new delays" << endl;
  //   delay0 = stoi(argv[1], NULL);
  //   delay1 = stoi(argv[2], NULL);
  //   delay2 = stoi(argv[3], NULL);
  //   cout << delay0 << " " << delay1 << " " << delay2 << endl;
  // }

  RFSOC pl; // instanciate the Programmable Logic interface.

  pl.set_delays(delay0, delay1, delay2);
  pl.sync_accumulators();
  pl.set_batch_sizes(batch_size);
  general_infos = pl.get_general_infos();
  general_infos.nb_channels = nb_channels;
  cout << "Frequency: " << general_infos.frequency << endl;

  pl.set_injection_status(true);

  cout << "\nDelayers: " << endl;
  for (int i = 0; i < 3; i++)
  {
    cout << "Channel " << i << ": " << general_infos.delays[i] << endl;
  }

  zmqpp::message message;
  message.add_raw(&general_infos, sizeof(GeneralInfos));
  socket.send(message);

  // while (*batch < 1)
  //   ;
  long counter = 0;
  while (true)
  {
    pl.update_data();
    for (int i = 0; i < 2; i++)
    {
      sigma_struct = pl.get_sigmas(i);
      sendZmqMultiMessage(&sigma_struct, sizeof(AccumulatorsSigma));
    }
    for (int j = 0; j < 1; j++)
    {
      correlation_struct = pl.get_corrs(j);
      sendZmqMultiMessage(&correlation_struct, sizeof(AccumulatorsCorr));
    }
    if ((counter + 3) % 1000 == 0 && capture_signal)
    {
      pl.set_injection_values(1ULL<<16, 1ULL<<16);
      pl.fifo_capture();
    }
    if ((counter + 2) % 1000 == 0 && capture_signal)
    {
      usleep(100);
      pl.set_injection_values(0, 0);
    }
    if (counter % 1000 == 0)
    {
      cout << "\rPackets sent: " << counter << endl;
      if (capture_signal)
      {
        pl.read_fifo();
        fifo_struct = pl.get_fifos();
        sendZmqMultiMessage(&fifo_struct, sizeof(FifoX2));
      }
    }
    counter++;
  }
}
