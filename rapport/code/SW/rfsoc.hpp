#ifndef RFSOC_H
#define RFSOC_H

//general config
#define MAX_CHANNELS 2
#define MAX_CORR 3
// FIFO addresses and offsets
#define FIFO_BASE 0xA00C0000
#define FIFO_BATCH_OFFSET 0
#define FIFO_A_LO_OFFSET 1
#define FIFO_A_HI_OFFSET 2
#define FIFO_B_LO_OFFSET 3
#define FIFO_B_HI_OFFSET 4
#define FIFO_CONTROL_OFFSET 10 
#define FIFO_CTL_CAP 0x01
#define FIFO_CTL_NEXT 0x02

#define FIFO_SAMPLE_TOTAL_COUNT 32768

// SIGMA addresses and offsets
#define SIGMA_BASE_0 0xA00D0000
#define SIGMA_BASE_1 0xA00D0000
#define SIGMA_BASE_2 0xA00D0000
#define SIGMA_BASE_3 0xA00D0000
#define SIGMA_BASE_4 0xA00D0000
#define SIGMA_BASE_5 0xA00D0000
// #define SIGMA_B_BASE 0xA0030000
#define SIGMA_MEAN_OFFSET 0x0
#define SIGMA_MEAN2_OFFSET 0x8

// Correlation addresses and offsets
#define CORR_BASE 0xA00E0000
#define CORR_SIZE_OFFSET 0x00
#define CORR_RESULT

#define SYSTEM_CTRL_BASE 0xA0160000

#endif